import express from "express";
import cors from "cors";
import { createID } from "./src/configs/random/random.js";

// import { initApiRouter } from "./src/route/router.js";
import { initApiRouter } from "./src/route/api.js";

const app = express();
const port = 8085;

app.use(express.json());
app.use(express.urlencoded({
	extended: true
}));
app.use(cors());
app.use(express.static("./"));
app.get('/', (req, res) => {
	const arr = [];
	for (let i = 0; i < 200; i++) {
		arr.push(createID(20))
	}
	const result = arr.reduce((value, item) => value += `${item}==`, "");
	res.send(
		"result" + "\n" + "sdfsđfj"
	);
})

initApiRouter(app);

app.listen(port, () => {
	console.log(`Example app listening on port ${port}`)
})