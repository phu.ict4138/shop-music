import express from "express";

import {
    getOne,
    getAll,
    edit,
    create,
    detete
} from "../controller/category.js";

const router = express.Router();

const categoryRouter = (app) => {

    router.get("/:id", getOne);
    router.get("/", getAll);
    router.put("/update", edit);
    router.post("/create", create);
    router.delete("/delete", detete);

    return app.use("/api/v1/category", router);
}

export default categoryRouter
