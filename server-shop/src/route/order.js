import express from "express";

const router = express.Router();

const orderRouter = (app) => {

    return app.use("/api/v1/order/", router);
}

export {
    orderRouter
}