import express from "express";

import {
    search,
    edit,
    create,
    getOne,
    detete
} from "../controller/product.js";

const router = express.Router();

const productRouter = (app) => {

    router.post("/search", search);
    router.put("/update", edit);
    router.get("/:id", getOne);
    router.post("/create", create);
    router.delete("/delete", detete);

    return app.use("/api/v1/product", router);
}

export default productRouter
