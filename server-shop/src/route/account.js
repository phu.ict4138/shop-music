import express from "express";

import {
    login,
    signup,
    history
} from "../controller/account.js";

const router = express.Router();

const accountRouter = (app) => {

    router.post("/login", login);
    router.post("/signup", signup);
    router.post("/history", history);

    return app.use("/api/v1/account", router);
}

export default accountRouter
