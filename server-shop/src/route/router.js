import accountRouter from "./account.js";
import productRouter from "./product.js";
import categoryRouter from "./category.js";
import originRouter from "./origin.js";

const initApiRouter = (app) => {
    accountRouter(app);
    productRouter(app);
    categoryRouter(app);
    originRouter(app);
}

export {
    initApiRouter
}