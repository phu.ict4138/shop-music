import express from "express";

import {
    getAllCategory,

    getAllOrigin,

    getAllProduct,
    getProduct,

    login,
    signup,
    getUser,
    getHistory,

    getCart,
    increase,
    decrease,

    createOrder,
    search
} from "../controller/apiController.js";

const router = express.Router();

const initApiRouter = (app) => {
    router.get("/category/get-all", getAllCategory);
    router.get("/category/get-one", getAllCategory);


    router.get("/origin/get-all", getAllOrigin);

    router.get("/product/get-all", getAllProduct);
    router.get("/product/:id", getProduct);


    router.post("/user/login", login);
    router.post("/user/signup", signup);
    router.get("/user/:id", getUser);
    router.post("/user/history", getHistory);


    router.post("/cart/get-cart", getCart);
    router.post("/cart/increase", increase);
    router.post("/cart/decrease", decrease);

    router.post("/order/create", createOrder);

    router.get("/search", search);

    return app.use("/api/v1/", router);
}

export {
    initApiRouter
};