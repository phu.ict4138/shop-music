import express from "express";

const router = express.Router();

const cartRouter = (app) => {

    return app.use("/api/v1/cart/", router);
}

export {
    cartRouter
}