import express from "express";

import {
    getOne,
    getAll,
    edit,
    create,
    detete
} from "../controller/origin.js";

const router = express.Router();

const originRouter = (app) => {

    router.get("/:id", getOne);
    router.get("/", getAll);
    router.put("/update", edit);
    router.post("/create", create);
    router.delete("/delete", detete);

    return app.use("/api/v1/origin", router);
}

export default originRouter
