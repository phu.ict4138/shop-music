const product = [
    {
        "id": 1,
        "tên": "Đàn guitar Taylor 414C",
        "giá": "66.400.000 VND",
        "giá mới": "",
        "danh mục": "Đàn Guitar Acoustic",
        "hãng": {
            "tên": "Taylor",
            "logo": ""
        },
        "hình ảnh": [

        ],
        "video": [

        ],
        "thông tin chi tiết": {
            "xuất xứ": ""
        }
    },
    {
        "id": 2,
        "tên": "Đàn Guitar Thuận AT-01C",
        "giá": "3.900.000 VND",
        "giá mới": "2.990.000 VND",
        "danh mục": "Đàn Guitar Acoustic",
        "hãng": {
            "tên": "Guitar Thuận",
            "logo": ""
        },
        "hình ảnh": [
            
        ],
        "video": [

        ],
        "thông tin": {
            "Xuất xứ": "Việt Nam",
            "Kiểu dáng": "A khuyết",
            "Kiểu sơn": "Sơn nhám",
            "Mặt đàn": "Gỗ Thông Sitka nguyên tấm",
            "Lưng/Hông": "Gỗ Hồng đào",
            "Đầu/Cần đàn": "Gỗ Mahogany",
            "Ngựa đàn": "Gỗ Cẩm Lai",
            "Dây đàn": "Alice A436",
            "Ty chỉnh cần": "Có"
        }
    },
    {
        "id": 3,
        "tên": "Đàn Guitar Thuận AT-01",
        "giá": "3.200.000 VND",
        "giá mới": "3.000.000 VND",
        "danh mục": "Đàn Guitar Acoustic",
        "hãng": {
            "tên": "Guitar Thuận",
            "logo": ""
        },
        "hình ảnh": [
            
        ],
        "video": [

        ],
        "thông tin": {
            "Xuất xứ": "Việt Nam",
            "Kiểu dáng": "AO Full",
            "Kiểu sơn": "Sơn mộc",
            "Mặt đàn": "Gỗ Thông Sitka nguyên tấm",
            "Lưng/Hông": "Gỗ Hồng đào",
            "Đầu/Cần đàn": "Gỗ Mahogany, Cần đàn liền nguyên khối",
            "Ngựa đàn": "Gỗ Cẩm Lai",
            "Dây đàn": "Alice A436",
            "Ty chỉnh cần": "Có"
        }
    },
]