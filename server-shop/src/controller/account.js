import { connection } from "../configs/database/connection.js";

const login = async (req, res) => {

    const { username, password } = req.body;
    if (!username || !password) {
        return res.status(400).json({
            result: "fail",
            message: "Không đủ tham số"
        });
    }
    const [result] = await connection.execute(
        `select * from user where username = ? or email = ? or phone = ?`,
        [username, username, username]
    );
    if (result.length == 0) {
        return res.status(400).json({
            result: "fail",
            message: "Tài khoản không tồn tại"
        });
    }
    if (result[0].password != password) {
        return res.status(400).json({
            result: "fail",
            message: "Sai mật khẩu"
        });
    }
    const account = result[0];
    account.password = undefined;
    return res.status(200).json({
        result: "success",
        account: account
    });
}

const signup = async (req, res) => {

    const { username, password } = req.body;
    if (!username || !password) {
        return res.status(400).json({
            result: "fail",
            message: "Không đủ tham số"
        });
    }
    const phone = req.body.phone ?? null;
    const email = req.body.email ?? null;
    const [checkUsername] = await connection.execute(
        `select * from user where username = ?`,
        [username]
    );
    if (checkUsername.length != 0) {
        return res.status(400).json({
            result: "fail",
            message: "Tên đăng nhập đã tồn tại"
        });
    }
    if (phone) {
        const [checkPhone] = await connection.execute(
            `select * from user where phone = ?`,
            [phone]
        );
        if (checkPhone.length != 0) {
            return res.status(400).json({
                result: "fail",
                message: "Só điện thoại đã được sử dụng"
            });
        }
    }
    if (email) {
        const [checkEmail] = await connection.execute(
            `select * from user where email = ?`,
            [email]
        );
        if (checkEmail.length != 0) {
            return res.status(400).json({
                result: "fail",
                message: "Email đã được sử dụng"
            });
        }
    }
    const fullname = req.body.fullname ?? null;
    const address = null;
    const birthday = "2022-02-02 00:00:00";
    const avatar = "upload/images/avatar_default.png";
    const [result] = await connection.execute(
        `insert into 
            user(
                username, 
                password, 
                fullname, 
                phone,
                email,
                address,
                birthday,
                avatar
            ) values(?, ?, ?, ?, ?, ?, ?, ?)`,
        [username, password, fullname, phone, email, address, birthday, avatar]
    );
    const [result_2] = await connection.execute(
        `select * from user where id =?`,
        [result.insertId]
    );
    const account = result_2[0];
    account.password = undefined;
    return res.status(200).json({
        result: "success",
        account: account
    });
}

const history = async (req, res) => {

    const { userId } = req.body;
    if (!userId) {
        return res.status(400).json({
            result: "fail",
            message: "Không đủ tham số"
        });
    }
    const page = req.body.page ?? 0;
    const limit = req.body.limit ?? 10;
    const [result] = await connection.execute(
        `select * from orders where id_user = ? limit ? offset ?`,
        [userId, limit, page * limit]
    );

    return res.status(200).json({
        result: "success",
        history: result
    });
}

export {
    login,
    signup,
    history
}