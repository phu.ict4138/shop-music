import { connection } from "../configs/database/connection.js";

const getTime = () => {
    const date = new Date();
    const day = date.getDate();
    const month = date.getMonth() + 1;
    const year = date.getFullYear();
    const second = date.getSeconds();
    const minute = date.getMinutes();
    const hour = date.getHours();
    return `${year}-${month}-${day} ${hour}:${minute}:${second}`;
}

const getAllCategory = async (req, res) => {

    const [x, y] = await connection.execute(
        "select * from category"
    );

    return res.status(200).json({
        message: "success",
        category: x
    });
}

const getAllOrigin = async (req, res) => {

    const [x, y] = await connection.execute(
        "select * from origin"
    );

    return res.status(200).json({
        message: "success",
        origin: x
    });
}

const getAllProduct = async (req, res) => {

    const [x, y] = await connection.execute(
        `select p.id, p.name as pname, p.price, p.newprice, p.country as pcountry, p.image, p.description as pdes, 
        c.id as category, c.name as cname, c.description as cdes, c.logo as clogo, 
        o.id as origin, o.name as oname, o.description as odes, o.country as ocountry, o.logo as ologo
        from product p, category c, origin o 
        where p.id_category = c.id and p.id_origin = o.id and p.image is not null`
    );

    const data = x.map(({ id, pname, price, newprice, pcountry, image, pdes, category, cname, cdes, clogo, origin, oname, odes, ocountry, ologo }) => ({
        id: id,
        name: pname,
        price,
        newprice,
        country: pcountry,
        image,
        description: pdes,
        category: {
            id: category,
            name: cname,
            logo: clogo,
            description: cdes
        },
        origin: {
            id: origin,
            name: oname,
            description: odes,
            country: ocountry,
            logo: ologo
        }
    }));

    return res.status(200).json({
        message: "success",
        product: data
    });
}

const getProduct = async (req, res) => {
    try {
        const { id } = req.params;
        const [x, y] = await connection.execute(
            `select p.id, p.name as pname, p.price, p.newprice, p.country as pcountry, p.image, p.description as pdes, 
        c.id as category, c.name as cname, c.description as cdes, c.logo as clogo, 
        o.id as origin, o.name as oname, o.description as odes, o.country as ocountry, o.logo as ologo
        from product p, category c, origin o 
        where p.id_category = c.id and p.id_origin = o.id and p.id = ?`,
            [id]
        );
        const [_x, _y] = await connection.execute(
            `select * from images where id_resource = ? order by id asc`,
            [id]
        );
        const __x = x[0];
        const product = {
            id: __x.id,
            name: __x.pname,
            price: __x.price,
            newprice: __x.newprice,
            country: __x.pcountry,
            image: __x.image,
            images: _x.map(({ link }) => link),
            description: __x.pdes,
            category: {
                id: __x.category,
                name: __x.cname,
                logo: __x.clogo,
                description: __x.cdes
            },
            origin: {
                id: __x.origin,
                name: __x.oname,
                description: __x.odes,
                country: __x.ocountry,
                logo: __x.ologo
            }
        }
        return res.status(200).json({
            message: "success",
            product: product
        });
    } catch (error) {
        return res.status(500).json("Loi he thong")
    }

}

const signup = async (req, res) => {
    try {
        const body = req.body;
        if (body.password != body.confirmPassword) return res.status(400).json({
            message: "fail",
            reason: "Không trùng mật khẩu"
        });
        const [x, y] = await connection.execute(
            `select * from user where username = ?`,
            [body.username]
        );
        if (x.length > 0) {
            return res.status(400).json({
                message: "fail",
                reason: "Tên đăng nhập đã tồn tại"
            });
        }
        const time = getTime();
        body.fullname = body.fullname ? body.fullname : null;
        body.phone = body.phone ? body.phone : null;
        body.email = body.email ? body.email : null;
        body.address = body.address ? body.address : "Hà Nội";
        body.birthday = body.birthday ? body.birthday : time;
        body.avatar = body.avatar ? body.avatar : "upload/images/avatar_default.png";
        await connection.execute(
            `INSERT INTO user (username, password, fullname, phone, email, address, birthday, avatar) VALUES (?, ?, ?, ?, ?, ?, ?, ?);`,
            [body.username, body.password, body.fullname, body.phone, body.email, body.address, body.birthday, body.avatar]
        );

        const [ansx, ansy] = await connection.execute(
            `select * from user where username = ?`,
            [body.username]
        );
        // ansx[0].password = undefined;
        return res.status(201).json({
            message: "success",
            user: ansx[0]
        });
    } catch (error) {
        console.log(error);
        return res.status(500).json({
            message: "fail",
            reason: "Lỗi hệ thống"
        })
    }
}

const login = async (req, res) => {
    try {
        const { username, password } = req.body;
        console.log(username, password)
        const [x, y] = await connection.execute(
            `select * from user where username = ?`,
            [username]
        );
        if (x.length > 0 && password == x[0]?.password) {
            // x[0].password = undefined;
            return res.status(200).json({
                message: "success",
                user: x[0]
            });
        }
        else {
            return res.status(400).json({
                message: "fail",
                reason: "Sai tên đăng nhập hoặc mật khẩu"
            });
        }
    } catch (error) {
        console.log(error);
        return res.status(500).json("Loi he thong")
    }
}

const getUser = async (req, res) => {
    const { id } = req.params;
    const [x, y] = await connection.execute(
        `select * from user where id = ?`,
        [id]
    );
    if (x.length > 0) {
        return res.status(200).json({
            message: "success",
            user: x[0]
        })
    }
    else {
        return res.status(400).json({
            message: "fail",
            reason: "unknown"
        })
    }
}

const getCart = async (req, res) => {
    const { id } = req.body;
    if (!id) {
        return res.status(404).json({
            message: "fail",
            reason: "Không đủ tham số"
        });
    }
    const [x, y] = await connection.execute(
        `select ca.id, ca.count, u.id as uid, u.username, u.fullname,
            p.id as pid, p.name, p.price, p.newprice, p.image from cart ca,user u, product p 
            where ca.id_product = p.id and ca.id_user = u.id and u.id = ?`,
        [id]
    );
    const data = x.map(item => ({
        id: item.id,
        user: {
            id: item.uid,
            name: item.fullname,
            username: item.username
        },
        product: {
            id: item.pid,
            name: item.name,
            price: item.price,
            newprice: item.newprice,
            image: item.image
        },
        count: item.count
    }));
    return res.status(200).json({
        message: "success",
        cart: data
    })
}

const getHistory = async (req, res) => {
    const { id } = req.body;
    const [x, y] = await connection.execute(
        `select * from orders where id_user=?`,
        [id]
    );
    const _x = [];
    for (let item of x) {
        const [__x, __y] = await connection.execute(
            `select * from order_detail where id_order = ?`,
            [item.id]
        );
        const _a = [];
        for (let i of __x) {
            const [_m, _n] = await connection.execute(
                `select * from product where id = ?`,
                [i.id_product]
            );
            _a.push({
                ..._m[0],
                count: i.count
            });
        }
        console.log(_a);
        _x.push({
            ...item,
            products: _a
        })
    }
    console.log(_x)
    return res.status(200).json({
        message: "success",
        history: _x
    })
}

//UPDATE `cart` SET `count` = '5' WHERE `cart`.`id` = 6;
const increase = async (req, res) => {
    const { id_user, id_product, number } = req.body;
    if (!id_user || !id_product || !number) {
        return res.status(400).json({
            message: "fail",
            reason: "Không đủ tham số"
        });
    }
    const [x, y] = await connection.execute(
        `select * from cart where id_user = ? and id_product = ?`,
        [id_user, id_product]
    );
    if (x.length <= 0) {
        const [_x, _y] = await connection.execute(
            `insert into cart(id_user, id_product, count) values (?, ?, ?)`,
            [id_user, id_product, number]
        );
    }
    else {
        const __x = x[0];
        const [___x, ___y] = await connection.execute(
            `update cart set count = ? where cart.id = ?`,
            [__x.count + number, __x.id]
        )
    }
    return res.status(200).json({
        message: "success"
    });
}

const decrease = async (req, res) => {
    const { id_user, id_product, number } = req.body;
    if (!id_user || !id_product || !number) {
        return res.status(400).json({
            message: "fail",
            reason: "Không đủ tham số"
        });
    }
    const [x, y] = await connection.execute(
        `select * from cart where id_user = ? and id_product = ?`,
        [id_user, id_product]
    );
    if (x[0].count - number <= 0) {
        const [_x, __y] = await connection.execute(
            `delete from cart where id = ?`,
            [x[0].id]
        )
    }
    else {
        const [___x, ___y] = await connection.execute(
            `update cart set count = ? where cart.id = ?`,
            [x[0].count - number, x[0].id]
        )
    }
    return res.status(200).json({
        message: "success"
    });
}

const createOrder = async (req, res) => {
    const { id, name, phone, address, note } = req.body;
    console.log(req.body)
    const [x, y] = await connection.execute(
        `select ca.id, ca.count, u.id as uid, u.username, u.fullname,
            p.id as pid, p.name, p.price, p.newprice, p.image from cart ca,user u, product p 
            where ca.id_product = p.id and ca.id_user = u.id and u.id = ?`,
        [id]
    );
    const data = x.map(item => ({
        id: item.id,
        user: {
            id: item.uid,
            name: item.fullname,
            username: item.username
        },
        product: {
            id: item.pid,
            name: item.name,
            price: item.price,
            newprice: item.newprice,
            image: item.image
        },
        count: item.count
    }));
    const total = data.reduce((pre, item) => pre + item.product.newprice * item.count, 0);
    console.log(total);
    const time = getTime();
    const [_x, _y] = await connection.execute(
        `insert into orders(id_user, name, address, phone, note, total, status, createat) values (?,?,?,?,?,?,?,?)`,
        [id, name, address, phone, note, total, "Chờ xác nhận", time]
    );
    const id_order = _x.insertId;
    const __t = null;
    const __ = data.map(async (item) => {
        await connection.execute(
            `insert into order_detail(id_order, id_product, count, note) values (?,?,?,?)`,
            [id_order, item.product.id, item.count, __t]
        );
        return 1;
    })
    for (let i of x) {
        await connection.execute(
            `delete from cart where id =  ?`,
            [i.id]
        )
    }
    return res.status(200).json({
        message: "success"
    });
}

const search = async (req, res) => {
    const { q } = req.query;
    const [x, y] = await connection.execute(
        `select * from product where name like ? and image is not null`,
        [`%${q}%`]
    )
    return res.status(200).json({
        message: "success",
        count: x.length,
        data: x
    })
}

export {
    getAllCategory,

    getAllOrigin,


    getAllProduct,
    getProduct,

    login,
    signup,
    getUser,
    getHistory,

    getCart,
    increase,
    decrease,

    createOrder,

    search
}