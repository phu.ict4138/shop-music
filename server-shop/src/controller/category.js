import { connection } from "../configs/database/connection.js";
import { createID } from "../configs/random/random.js";

const getOne = async (req, res) => {

    const { id } = req.params;
    console.log(createID(20))

    const [result] = await connection.execute(
        `select * from category where id = ?`,
        [id]
    );
    if (result.length == 0) {
        return res.status(400).json({
            result: "fail",
            message: "Không tìm thấy thông tin"
        });
    }
    const category = result[0];
    return res.status(200).json({
        result: "success",
        category: category
    });
}

const getAll = async (req, res) => {
    const [categories] = await connection.execute(
        `select * from category`
    ) ?? [] ?? req;

    return res.status(200).json({
        result: "success",
        total: categories.length,
        categories: categories
    });
}

const edit = async (req, res) => {

    const { categoryId } = req.body;
    if (!categoryId) {
        return res.status(400).json({
            result: "fail",
            message: "Không đủ tham số"
        });
    }
    const [result] = await connection.execute(
        `select * from category where id = ?`,
        [categoryId]
    );
    if (result.length == 0) {
        return res.status(400).json({
            result: "fail",
            message: "Không có danh mục này"
        });
    }
    const category_ = result[0];
    const name = req.body.name ?? category_.name ?? null;
    const description = req.body.description ?? category_.description ?? null;
    const logo = req.body.logo ?? category_.logo ?? null;

    await connection.execute(
        `update category 
        set name = ?,
            description = ?,
            logo = ?
        where id = ?`,
        [name, description, logo, categoryId]
    );
    const [result_2] = await connection.execute(
        `select * from category where id = ?`,
        [categoryId]
    );
    const category = result_2[0];

    return res.status(200).json({
        result: "success",
        category: category
    });
}

const create = async (req, res) => {
    const { name } = req.body;
    if (!name) {
        return res.status(400).json({
            result: "fail",
            message: "Không đủ tham số"
        });
    }
    const description = req.body.description ?? null;
    const logo = req.body.logo ?? null;
    const [result] = await connection.execute(
        `insert into category(name, description, logo) values (?, ?, ?)`,
        [name, description, logo]
    );
    const [result_2] = await connection.execute(
        `select * from category where id = ?`,
        [result.insertId]
    );
    const category = result_2[0];
    return res.status(200).json({
        result: "success",
        category: category
    });
}

const detete = async (req, res) => {

    const { categoryIds } = req.body;
    if (!categoryIds || categoryIds.length == 0) {
        return res.status(400).json({
            result: "fail",
            message: "Không đủ tham số"
        });
    }
    for (const item of categoryIds) {
        await connection.execute(
            `delete from category where id = ?`,
            [item]
        );
    }

    return res.status(200).json({
        result: "success",
        message: "Xóa thành công"
    });
}

export {
    getOne,
    getAll,
    edit,
    create,
    detete
}