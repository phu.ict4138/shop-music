import { connection } from "../configs/database/connection.js";

const search = async (req, res) => {
    try {
        const query = req.body.query ?? "";
        const page = req.body.page ?? 0;
        const limit = req.body.limit ?? 10;
        const [result] = await connection.execute(
            `select * from product where name like '%${query}%' limit ? offset ?`,
            [limit, page * limit]
        );
        for (const item of result) {
            const [category] = await connection.execute(
                `select * from category where id = ?`,
                [item.id_category]
            );
            const [origin] = await connection.execute(
                `select * from origin where id = ?`,
                [item.id_origin]
            );
            const [images] = await connection.execute(
                `select link from images where id_resource = ?`,
                [item.id]
            );
            item.category = category[0] ?? {};
            item.origin = origin[0] ?? {};
            item.images = images.map(item => item.link);
            item.id_category = undefined;
            item.id_origin = undefined;
        }
        const [count] = await connection.execute(
            `select * from product where name like '%${query}%'`
        );
        const total = count.length;
        return res.status(200).json({
            result: "success",
            total: total,
            page: page,
            limit: limit,
            products: result
        });
    }
    catch (error) {
        return res.status(500).json({
            result: "fail",
            message: "Lỗi hệ thống"
        });
    }
}

const edit = async (req, res) => {
    try {
        const { productId } = req.body
        console.log(req.body)
        const [result] = await connection.execute(
            `select * from product where id = ?`,
            [productId]
        );
        if (result.length == 0) {
            return res.status(400).json({
                result: "fail",
                message: "Không có sản phẩm này"
            });
        }
        const product = result[0];
        const name = req.body.name ?? product.name ?? null;
        const price = req.body.price ?? product.price ?? null;
        const newPrice = req.body.newPrice ?? product.newPrice ?? null;
        const country = req.body.country ?? product.country ?? null;
        const description = req.body.description ?? product.description ?? null;
        const image = req.body.image ?? product.image ?? null;
        const idCategory = req.body.idCategory ?? product.id_category ?? null;
        const idOrigin = req.body.idOrigin ?? product.id_origin ?? null;
        await connection.execute(
            `update product 
            set name = ?,
                price = ?,
                newPrice = ?,
                country = ?,
                description = ?,
                image = ?,
                id_category = ?,
                id_origin = ?
            where id = ?`,
            [name, price, newPrice, country, description, image, idCategory, idOrigin, product.id]
        );
        const [result_2] = await connection.execute(
            `select * from product where id = ?`,
            [productId]
        );
        const product_2 = result_2[0];
        const [category] = await connection.execute(
            `select * from category where id = ?`,
            [product_2.id_category]
        );
        const [origin] = await connection.execute(
            `select * from origin where id = ?`,
            [product_2.id_origin]
        );
        const [images] = await connection.execute(
            `select link from images where id_resource = ?`,
            [product_2.id]
        );
        product_2.category = category[0] ?? {};
        product_2.origin = origin[0] ?? {};
        product_2.images = images.map(item => item.link);
        product_2.id_category = undefined;
        product_2.id_origin = undefined;

        return res.status(200).json({
            result: "success",
            product: product_2
        });
    }
    catch {
        console.log(error);
        return res.status(500).json({
            result: "fail",
            message: "Lỗi hệ thống"
        });
    }
}

const create = async (req, res) => {
    try {
        const name = req.body.name;
        const idCategory = req.body.id_category;
        const idOrigin = req.body.id_origin;
        if (!name || !idCategory || !idOrigin) {
            return res.status(400).json({
                result: "fail",
                message: "Không đủ tham số"
            });
        }
        const price = req.body.price ?? null;
        const newPrice = req.body.newPrice ?? null;
        const country = req.body.country ?? null;
        const description = req.body.description ?? null;
        const image = req.body.image ?? null;
        const [result] = await connection.execute(
            `insert into 
            product(
                name, 
                id_category, 
                id_origin, 
                price, 
                newPrice, 
                country, 
                description, 
                image
            ) values (?, ?, ?, ?, ?, ?, ?, ?)`,
            [name, idCategory, idOrigin, price, newPrice, country, description, image]
        );
        const [result_2] = await connection.execute(
            `select * from product where id = ?`,
            [result.insertId]
        );
        const product = result_2[0];
        const [category] = await connection.execute(
            `select * from category where id = ?`,
            [product.id_category]
        );
        const [origin] = await connection.execute(
            `select * from origin where id = ?`,
            [product.id_origin]
        );
        const [images] = await connection.execute(
            `select link from images where id_resource = ?`,
            [product.id]
        );
        product.category = category[0] ?? {};
        product.origin = origin[0] ?? {};
        product.images = images.map(item => item.link);
        product.id_category = undefined;
        product.id_origin = undefined;
        return res.status(200).json({
            result: "success",
            product: product
        });
    }
    catch (error) {
        console.log(error);
        return res.status(500).json({
            result: "fail",
            message: "Lỗi hệ thống"
        });
    }
}

const detete = async (req, res) => {
    try {
        const { productIds } = req.body;
        console.log(productIds)
        if (!productIds || productIds.length == 0) {
            return res.status(400).json({
                result: "fail",
                message: "Không đủ tham số"
            });
        }
        for (const item of productIds) {
            await connection.execute(
                `delete from product where id = ?`,
                [item]
            );
        }
        return res.status(200).json({
            result: "success",
            message: "Xóa thành công"
        });
    }
    catch (error) {
        console.log(error);
        return res.status(500).json({
            result: "fail",
            message: "Lỗi hệ thống"
        });
    }
}

const getOne = async (req, res) => {
    try {
        const { id } = req.params;
        const [result] = await connection.execute(
            `select * from product where id = ?`,
            [id]
        );
        if (result.length == 0) {
            return res.status(400).json({
                result: "fail",
                message: "Không tìm thấy thông tin"
            });
        }
        const product = result[0];
        const [category] = await connection.execute(
            `select * from category where id = ?`,
            [product.id_category]
        );
        const [origin] = await connection.execute(
            `select * from origin where id = ?`,
            [product.id_origin]
        );
        const [images] = await connection.execute(
            `select link from images where id_resource = ?`,
            [product.id]
        );
        product.category = category[0] ?? {};
        product.origin = origin[0] ?? {};
        product.images = images.map(item => item.link);
        product.id_category = undefined;
        product.id_origin = undefined;
        return res.status(200).json({
            result: "success",
            product: product
        });
    }
    catch (error) {
        return res.status(500).json({
            result: "fail",
            message: "Lỗi hệ thống"
        });
    }
}

export {
    search,
    edit,
    getOne,
    create,
    detete
}