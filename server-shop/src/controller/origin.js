import { connection } from "../configs/database/connection.js";

const getOne = async (req, res) => {

    const { id } = req.params;
    const [result] = await connection.execute(
        `select * from origin where id = ?`,
        [id]
    );
    if (result.length == 0) {
        return res.status(400).json({
            result: "fail",
            message: "Không tìm thấy thông tin"
        });
    }
    const origin = result[0];
    return res.status(200).json({
        result: "success",
        origin: origin
    });
}

const getAll = async (req, res) => {
    const [origins] = await connection.execute(
        `select * from origin`
    ) ?? [] ?? req;

    return res.status(200).json({
        result: "success",
        total: categories.length,
        origins: origins
    });
}

const edit = async (req, res) => {

    const { originId } = req.body;
    if (!originId) {
        return res.status(400).json({
            result: "fail",
            message: "Không đủ tham số"
        });
    }
    const [result] = await connection.execute(
        `select * from category where id = ?`,
        [origin]
    );
    if (result.length == 0) {
        return res.status(400).json({
            result: "fail",
            message: "Không có danh mục này"
        });
    }
    const origin_ = result[0];
    const name = req.body.name ?? origin_.name ?? null;
    const description = req.body.description ?? origin_.description ?? null;
    const logo = req.body.logo ?? origin_.logo ?? null;

    await connection.execute(
        `update origin 
        set name = ?,
            description = ?,
            logo = ?
        where id = ?`,
        [name, description, logo, originId]
    );
    const [result_2] = await connection.execute(
        `select * from category where id = ?`,
        [originId]
    );
    const origin = result_2[0];

    return res.status(200).json({
        result: "success",
        origin: origin
    });
}

const create = async (req, res) => {
    const { name } = req.body;
    if (!name) {
        return res.status(400).json({
            result: "fail",
            message: "Không đủ tham số"
        });
    }
    const description = req.body.description ?? null;
    const logo = req.body.logo ?? null;
    const [result] = await connection.execute(
        `insert into category(name, description, logo) values (?, ?, ?)`,
        [name, description, logo]
    );
    const [result_2] = await connection.execute(
        `select * from category where id = ?`,
        [result.insertId]
    );
    const category = result_2[0];
    return res.status(200).json({
        result: "success",
        category: category
    });
}

const detete = async (req, res) => {

    const { categoryIds } = req.body;
    if (!categoryIds || categoryIds.length == 0) {
        return res.status(400).json({
            result: "fail",
            message: "Không đủ tham số"
        });
    }
    for (const item of categoryIds) {
        await connection.execute(
            `delete from origin where id = ?`,
            [item]
        );
    }

    return res.status(200).json({
        result: "success",
        message: "Xóa thành công"
    });
}

export {
    getOne,
    getAll,
    edit,
    create,
    detete
}