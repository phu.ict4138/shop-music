const arr = 'abcdefghijklmnopqrstuvwxyz1234567890';

const randomIndex = () => {
    return Math.floor(Math.random() * arr.length);
}

const createID = (len = 10) => {
    let id = '';
    for (let i = 0; i < len; i++) {
        id = id + arr[randomIndex()];
    }
    return id;
}

export {
    createID
}