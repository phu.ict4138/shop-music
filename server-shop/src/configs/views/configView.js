import express from "express";

const configView = (app) => {
    app.use(express.static("./src/public"));
}

export {
    configView
}