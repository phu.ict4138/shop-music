import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import classNames from "classnames/bind";

import styles from "./Header.module.scss";
import Navbar from "../Navbar";
import image_1 from "../../assets/images/anh1.jpg";
import CartButton from "../CartButton";
import Global from "../../global";
import { BASE_URL } from "../../configs/config";

const cx = classNames.bind(styles);

function Header() {

    const navigate = useNavigate();
    const [searchValue, setSearchValue] = useState("");
    Global.setFunc("setSearchValue", setSearchValue);
    const user = Global.getUser()

    console.log("re-render header")

    return (
        <div className={cx("wrapper")}>
            <div className={cx("top")}>
                {!user?.fullname ?
                    (<div>
                        <span
                            className={cx("login")}
                            onClick={() => navigate("./signup")}>
                            Đăng ký
                        </span>
                        <span style={{ marginLeft: 8, marginRight: 8, color: "white" }}>|</span>
                        <span
                            className={cx("login")}
                            onClick={() => navigate("./login")}>
                            Đăng nhập
                        </span>
                    </div>)
                    :
                    (<div className={cx("action")}>
                        <img
                            src={user.avatar ? BASE_URL + user.avatar : image_1}
                            className={cx("img")}
                            onClick={() => navigate("./account/profile")} />
                        <span
                            className={cx("username")}
                            onClick={() => navigate("./account/profile")}>
                            {user.fullname}
                        </span>
                        <span
                            className={cx("login")}
                            onClick={() => {
                                Global.setUser({});
                                Global.getFunc("setNumberCart")(0);
                                localStorage.clear("user");
                                navigate("/");
                                Global.getFunc("renderApp")(Math.random());
                            }}>
                            {`- Đăng xuất`}
                        </span>
                    </div>)
                }
            </div>
            <div className={cx("inner")}>
                <h1
                    className={cx("logo")}
                    onClick={() => navigate("/")}>
                    P-MUSIC
                </h1>
                <div className={cx("search")}>
                    <input
                        className={cx("search-input")}
                        placeholder="Tìm kiếm..."
                        value={searchValue}
                        onChange={e => setSearchValue(e.target.value)}
                        onKeyDown={e => {
                            if (e.key == "Enter") {
                                navigate(`./search?q=${searchValue}`);
                            }
                        }} />
                    <button
                        className={cx("btn-search")}
                        onClick={() => navigate(`./search?q=${searchValue}`)}>
                        <FontAwesomeIcon icon={faMagnifyingGlass} />
                    </button>
                </div>
                <div className={cx("left")}>
                    <CartButton />
                </div>
            </div>
            <div className={cx("navbar")}>
                <Navbar />
            </div>
        </div>
    );
}

export default Header