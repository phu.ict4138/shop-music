import { useNavigate } from "react-router-dom";
import classNames from "classnames/bind";

import styles from "./Navbar.module.scss";

const cx = classNames.bind(styles);

const navlist = [
    { display: "Trang chủ", path: "/" },
    { display: "Bộ sưu tập", path: "/product" },
    { display: "Liên hệ", path: "/" },
]

function Navbar() {

    const navigate = useNavigate();

    return (
        <div className={cx("wrapper")}>
            {navlist.map((item) => (
                <li
                    className={cx("navbar-item")}
                    key={item.display}
                    onClick={() => navigate(item.path)} >
                    {item.display}
                </li>
            ))}
        </div>
    );
}

export default Navbar