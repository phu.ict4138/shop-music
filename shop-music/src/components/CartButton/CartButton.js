import classNames from "classnames/bind";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCartShopping } from "@fortawesome/free-solid-svg-icons";

import styles from "./CartButton.module.scss";
import Global from "../../global";
import { BASE_URL } from "../../configs/config";

const cx = classNames.bind(styles);

function CartButton() {

    const navigate = useNavigate();
    const [number, setNumber] = useState(0);
    const user = Global.getUser();
    Global.setFunc("setNumberCart", setNumber);

    const loadData = async () => {
        const tmp = Global.getUser();
        if (!tmp.id) {
            return;
        }
        await fetch(`${BASE_URL}api/v1/cart/get-cart`, {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                id: tmp.id
            })
        })
            .then(res => res.json())
            .then(res => {
                setNumber(res.cart.length)
            });
    }

    Global.setFunc("loadCartCount", loadData);

    useEffect(() => {
        loadData();
    }, [])

    return (
        <div className={cx("wrapper")}>
            <button
                className={cx("btn-cart")}
                onClick={() => navigate("./cart")} >
                <FontAwesomeIcon icon={faCartShopping} />
            </button>
            <button className={cx("dot")}>{number}</button>
        </div>
    );
}

export default CartButton