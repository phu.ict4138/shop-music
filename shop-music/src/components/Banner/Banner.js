import { useNavigate } from "react-router";
import classNames from "classnames/bind";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import styles from "./Banner.module.scss";
import image_1 from "../../assets/images/anh1.jpg";
import image_2 from "../../assets/images/anh2.jpg";
import image_3 from "../../assets/images/anh3.jpg";
import image_4 from "../../assets/images/anh4.jpg";

const cx = classNames.bind(styles);
const data = [
    { id: 1, image: image_1 },
    { id: 2, image: image_2 },
    { id: 3, image: image_3 },
    { id: 4, image: image_4 },
]

function Banner() {

    const navigate = useNavigate();

    return (
        <div className={cx("wrapper")}>
            <div className={cx("slider")}>
                <Slider
                    dots={true}
                    infinite={true}
                    speed={1000}
                    autoplaySpeed={2000}
                    slidesToShow={1}
                    slidesToScroll={1}
                    autoplay={true}
                    appendDots={(dot) => <ul style={{ margin: 0, marginTop: 30 }}>{dot}</ul>} >
                    {data.map((item) => (
                        <img
                            className={cx('image')}
                            onClick={() => navigate("./product")}
                            key={item.id}
                            src={item.image}
                            alt={`${item.id}`} />
                    ))}
                </Slider>
            </div>
        </div>
    );
}

export default Banner