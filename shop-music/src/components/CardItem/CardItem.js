import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import classNames from "classnames/bind";

import styles from "./CardItem.module.scss";
import { concatString, money } from "../../configs/config";
import Global from "../../global";
import { BASE_URL } from "../../configs/config";

const cx = classNames.bind(styles);

function CardItem({ id, name, price, newPrice, image }) {

    const navigate = useNavigate();
    const [str, setStr] = useState(() => {
        if (typeof (name) != "undefined") {
            return concatString(name)
        }
        return "";
    });

    const user = Global.getUser();

    return (
        <div className={cx("wrapper")}>
            <div
                className={cx("content")}
                onClick={() => navigate(`/product/${str}${id}`)} >
                <img
                    className={cx("image")}
                    src={image} />
                <h4 className={cx("name")}>{name}</h4>
                <span className={cx("price")}>{`${money(price)} VND`}</span>
                <span className={cx("new-price")}>{`${money(newPrice)} VND`}</span>
                <div className={cx("sale")}>
                    <span className={cx("percent")}>{`${Math.floor(100 - newPrice / price * 100)}%`}</span>
                    <span className={cx("label")}>GIẢM</span>
                </div>
            </div>
            <button
                className={cx("btn-add")}
                onClick={async () => {
                    if (!user.username) {
                        window.alert("Bạn cần đăng nhâp");
                    }
                    else {
                        console.log("call api")
                        await fetch(`${BASE_URL}api/v1/cart/increase`, {
                            method: "POST",
                            headers: {
                                Accept: "application/json",
                                "Content-Type": "application/json"
                            },
                            body: JSON.stringify({
                                id_user: user.id,
                                id_product: id,
                                number: 1
                            })
                        })
                            .then(res => res.json())
                            .then(res => {
                                Global.getFunc("loadCartCount")();
                            })
                    }
                }} >
                <FontAwesomeIcon icon={faPlus} />
            </button>
        </div>
    );
}

export default CardItem