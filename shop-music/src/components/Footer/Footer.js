import { faFacebook, faInstagram, faTwitter } from "@fortawesome/free-brands-svg-icons";
import { faEnvelope, faLocationDot, faMobileButton } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import classNames from "classnames/bind";
import { Link } from "react-router-dom";

import styles from './Footer.module.scss';

const cx = classNames.bind(styles);

function Footer() {

    return (
        <div className={cx('wrapper')}>
            <div className={cx('inner')}>
                <div className={cx('about')}>
                    <h2>Cửa hàng</h2>
                    <span className={cx('name')}>K - MUSIC</span>
                    <div className={cx('address')}>
                        <FontAwesomeIcon style={{ width: 18 }} icon={faLocationDot} />
                        <span>  Hai Bà Trưng, Hà Nội</span>
                    </div>
                    <div className={cx('phone')}>
                        <FontAwesomeIcon style={{ width: 18 }} icon={faMobileButton} />
                        <span>  0987654321</span>
                    </div>
                    <div className={cx('mail')}>
                        <FontAwesomeIcon style={{ width: 18 }} icon={faEnvelope} />
                        <span>  google@gmail.com</span>
                    </div>
                </div>

                <div className={cx('policy')}>
                    <h2>Chính sách</h2>
                    <Link to='/'>Chính sách bán hàng</Link>
                    <Link to='/'>Chính sách đổi trả</Link>
                    <Link to='/'>Chính sách bảo mật</Link>
                </div>

                <div className={cx('guide')}>
                    <h2>Hướng dẫn</h2>
                    <Link to='/'>Hướng dẫn mua hàng</Link>
                    <Link to='/'>Hướng dẫn đổi trả</Link>
                </div>

                <div className={cx('connect')}>
                    <h2>Kết nối</h2>
                    <a href="https://www.facebook.com" target="_blank" rel="noreferrer">
                        <FontAwesomeIcon icon={faFacebook} />
                    </a>
                    <a href="https://www.instagram.com" target="_blank" rel="noreferrer">
                        <FontAwesomeIcon icon={faInstagram} />
                    </a>
                    <a href="https://www.twitter.com" target="_blank" rel="noreferrer">
                        <FontAwesomeIcon icon={faTwitter} />
                    </a>
                </div>

            </div>
        </div>
    );
}

export default Footer