import { faHeadset, faMoneyCheck, faShieldHalved, faTruck } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import classNames from "classnames/bind";

import styles from "./Information.module.scss";

const cx = classNames.bind(styles);

function Information() {

    return (
        <div className={cx("wrapper")}>
            <div className={cx("inner")}>
                <button className={cx("image")}>
                    <FontAwesomeIcon icon={faTruck} />
                </button>
                <h3 className={cx("title")}>Giao Hàng</h3>
                <span className={cx("content")}>Giao hàng nhanh chóng. Ship mọi miền tổ quốc</span>
            </div>
            <div className={cx("inner")}>
                <button className={cx("image")}>
                    <FontAwesomeIcon icon={faMoneyCheck} />
                </button>
                <h3 className={cx("title")}>Thanh Toán</h3>
                <span className={cx("content")}>Thanh toán khi nhận hàng. Kiểm tra hàng trước khi thanh toán</span>
            </div>
            <div className={cx("inner")}>
                <button className={cx("image")}>
                    <FontAwesomeIcon icon={faShieldHalved} />
                </button>
                <h3 className={cx("title")}>Bảo Mật</h3>
                <span className={cx("content")}>Bảo mật thông tin tuyệt đối</span>
            </div>
            <div className={cx("inner")}>
                <button className={cx("image")}>
                    <FontAwesomeIcon icon={faHeadset} />
                </button>
                <h3 className={cx("title")}>Hỗ Trợ</h3>
                <span className={cx("content")}>Hỗ trợ 24/7</span>
            </div>
        </div>
    );
}

export default Information