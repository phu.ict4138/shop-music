/* eslint-disable */
import { useNavigate } from "react-router-dom";
import { faCaretRight, faTableCellsLarge } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import classNames from "classnames/bind";

import styles from "./Category.module.scss";
import CategoryItem from "./CategoryItem";
import { useEffect, useState } from "react";
import { BASE_URL } from "../../configs/config.js";

const cx = classNames.bind(styles);

function Category() {

    const [categories, setCategories] = useState([]);

    const loadData = async () => {
        await fetch(`${BASE_URL}api/v1/category/get-all`, {
            method: "GET",
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        })
            .then(res => res.json())
            .then(res => {
                const arr = res.category.slice(0, 10).map(({ id, name, logo }) => ({
                    id,
                    name,
                    image: BASE_URL + logo
                }));
                setCategories(arr)
            })
    }

    useEffect(() => {
        loadData();
    }, []);

    const navigate = useNavigate();

    return (
        <div className={cx("wrapper")}>
            <div className={cx("header")}>
                <div className={cx("left")}>
                    <FontAwesomeIcon className={cx("icon-left")} icon={faTableCellsLarge} />
                    <h2 className={cx("title")}>Danh Mục Sản Phẩm</h2>
                </div>
                <button
                    className={cx("right")}
                    // onClick={() => loadData()}
                    onClick={() => navigate("./product")}
                >
                    <h4 className={cx("see-all")}>Xem tất cả</h4>
                    <FontAwesomeIcon className={cx("icon-right")} icon={faCaretRight} />
                </button>
            </div>
            <div className={cx("content")}>
                {categories.map((item) => (
                    <CategoryItem
                        key={item.id}
                        {...item} />
                ))}
            </div>
        </div>
    );
}

export default Category