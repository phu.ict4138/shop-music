import { useNavigate } from "react-router-dom";
import classNames from "classnames/bind";

import styles from "./CategoryItem.module.scss";
import image from "../../../assets/images/anh1.jpg";

const cx = classNames.bind(styles);

function CategoryItem({ id, name, image }) {

    const navigate = useNavigate();

    return (
        <div className={cx("wrapper")}>
            <img
                className={cx("image")}
                src={image} />
            <span className={cx("name")}>
                {name}
            </span>
        </div>
    );
}

export default CategoryItem