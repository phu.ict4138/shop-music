import { useNavigate } from "react-router-dom";
import { useState, useEffect } from "react";
import { faCaretRight, faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import classNames from "classnames/bind";

import styles from "./TopSale.module.scss";
import CardItem from "../CardItem";
import { BASE_URL } from "../../configs/config";

const cx = classNames.bind(styles);

function TopSale() {

    const navigate = useNavigate();

    const [products, setProducts] = useState([]);

    const loadData = async () => {
        await fetch(`${BASE_URL}api/v1/product/get-all`, {
            method: "GET",
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        })
            .then(res => res.json())
            .then(res => {
                const arr = res.product.slice(0, 6).map(item => ({
                    id: item.id,
                    name: item.name,
                    price: item.price || 200000,
                    newPrice: item.newprice || 150000,
                    image: BASE_URL + item.image
                }));
                setProducts(arr)
            })
    }

    useEffect(() => {
        loadData();
    }, []);

    return (
        <div className={cx("wrapper")}>
            <div className={cx("header")}>
                <div className={cx("left")}>
                    <FontAwesomeIcon className={cx("icon-left")} icon={faStar} />
                    <h2 className={cx("title")}>Top Sản Phẩm</h2>
                </div>
                <button
                    className={cx("right")}
                    onClick={() => navigate("./product")} >
                    <h4 className={cx("see-all")}>Xem tất cả</h4>
                    <FontAwesomeIcon className={cx("icon-right")} icon={faCaretRight} />
                </button>
            </div>
            <div className={cx("content")}>
                {products.map(item => (
                    <CardItem
                        key={item.id}
                        {...item} />
                ))}
            </div>
        </div>
    );
}

export default TopSale