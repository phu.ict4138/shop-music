import { useNavigate } from "react-router-dom";
import { useState, useEffect } from "react";
import { faCaretRight, faAtom } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import classNames from "classnames/bind";

import styles from "./Brand.module.scss";
import BrandItem from "./BrandItem";
import { BASE_URL } from "../../configs/config.js";

const cx = classNames.bind(styles);

function Brand() {

    const [brands, setBrands] = useState([]);

    const loadData = async () => {
        await fetch(`${BASE_URL}api/v1/origin/get-all`, {
            method: "GET",
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        })
            .then(res => res.json())
            .then(res => {
                const arr = res.origin.slice(0, 10).map(({ id, name, logo }) => ({
                    id,
                    name,
                    image: BASE_URL + logo
                }));
                setBrands(arr)
            });
    }

    useEffect(() => {
        loadData();
    }, []);

    const navigate = useNavigate();

    return (
        <div className={cx("wrapper")}>
            <div className={cx("header")}>
                <div className={cx("left")}>
                    <FontAwesomeIcon className={cx("icon-left")} icon={faAtom} />
                    <h2 className={cx("title")}>Thương Hiệu</h2>
                </div>
                <button
                    className={cx("right")}
                    onClick={() => navigate("./product")} >
                    <h4 className={cx("see-all")}>Xem tất cả</h4>
                    <FontAwesomeIcon className={cx("icon-right")} icon={faCaretRight} />
                </button>
            </div>
            <div className={cx("content")}>
                {brands.map(item => (
                    <BrandItem
                        key={item.id}
                        {...item} />
                ))}
            </div>
        </div>
    );
}

export default Brand