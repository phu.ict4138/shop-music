import { useNavigate } from "react-router-dom";
import classNames from "classnames/bind";

import styles from "./BrandItem.module.scss";
import image from "../../../assets/images/anh1.jpg";

const cx = classNames.bind(styles);

function BrandItem({ id, name, image }) {

    const navigate = useNavigate();

    return (
        <div className={cx("wrapper")}>
            <img
                className={cx("image")}
                src={image}
                alt={name} />
            <span className={cx("name")}>
                {name}
            </span>
        </div>
    );
}

export default BrandItem