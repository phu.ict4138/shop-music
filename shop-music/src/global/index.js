import _ from "lodash";

class Global {
    constructor() {
        this.user = {};
        this.func = {};
    }

    setUser(user) {
        this.user = { ...user };
    }

    getUser() {
        return this.user;
    }

    setFunc(name, func) {
        const tmp = this.func;
        _.set(tmp, name, func);
        this.func = { ...tmp };
    }

    getFunc(name) {
        const tmp = this.func;
        if (_.has(tmp, name)) {
            return tmp[name];
        }
        else {
            return undefined
        }
    }
}

export default new Global();

const tmp = {
    id: 2,
    username: "phu",
    password: "123456",
    fullname: "Trương Quang Phú",
    phone: "0987654321",
    email: "phu@gmail.com",
    address: "Hai Bà Trưng, Hà Nội",
    birthday: "2022-07-12T17:00:00.000Z",
    avatar: "upload/images/avatar_default.png"
};