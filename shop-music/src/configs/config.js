const BASE_URL = "http://localhost:8085/";

const logString = (text) => {
    window.alert("Log from config " + text);
}

const money = (num) => {
    return num.toLocaleString();
}

const concatString = (str) => {
    const arr = str.split(" ");
    const t = arr.reduce((pre, item) => pre + item + "-", "");
    return t;
}

export {
    BASE_URL,
    logString,
    money,
    concatString
}
