import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

import Header from "./components/Header";
import Home from "./pages/Home";
import Search from "./pages/Search";
import Product from "./pages/Product";
import Cart from "./pages/Cart";
import Account from "./pages/Account";
import Footer from "./components/Footer";
import Login from "./pages/Login";
import SignUp from "./pages/SignUp";
import CheckOut from "./pages/CheckOut";
import { useState } from "react";
import Global from "./global";

function App() {

    const [x, renderApp] = useState(1);
    Global.setFunc("renderApp", renderApp);
    console.log("re-render - appppp");
    const user = JSON.parse(localStorage.getItem("user") ?? "{}");
    Global.setUser(user);
    document.title = "P-Music";

    return (
        <Router>
            <div className="App">
                <Header />
                <div
                    style={{
                        display: "flex",
                        flexDirection: "column",
                        alignItems: "center",
                        background: "#f5f5f5" || x
                    }}>
                    <Routes>
                        <Route path="/" element={<Home />} />
                        <Route path="/search" element={<Search />} />
                        <Route path="/product/:name_product" element={<Product />} />
                        <Route path="/cart" element={<Cart />} />
                        <Route path="/account/:type" element={<Account />} />
                        <Route path="/login" element={<Login />} />
                        <Route path="/signup" element={<SignUp />} />
                        <Route path="/checkout" element={<CheckOut />} />
                    </Routes>
                </div>
                <Footer />
            </div>
        </Router>
    );
}

export default App;

