import classNames from "classnames/bind";

import styles from "./Home.module.scss";
import Banner from "../../components/Banner";
import FlashSale from "../../components/FlashSale";
import TopSale from "../../components/TopSale";
import Category from "../../components/Category";
import Brand from "../../components/Brand";
import Information from "../../components/Information/Information";

const cx = classNames.bind(styles);

function Home() {

    return (
        <div
            className={cx("container")}
            style={{

            }}>
            <Banner />
            <FlashSale />
            <TopSale />
            <Category />
            <Brand />
            <Information />
        </div>
    );
}

export default Home