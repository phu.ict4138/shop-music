import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { faMinus, faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import classNames from "classnames/bind";

import styles from "./Cart.module.scss";
import { BASE_URL, concatString, money } from "../../configs/config";
import Global from "../../global";

const cx = classNames.bind(styles);

function Cart() {

    const [cart, setCart] = useState([]);
    const navigate = useNavigate();
    const user = Global.getUser();

    console.log("re-render Cart")

    const total = () => {
        return cart.reduce((pre, item) => pre + item.product.newprice * item.count, 0);
    }

    useEffect(() => {
        Global.getFunc("loadCartCount")();
    })

    useEffect(() => {
        window.scrollTo(0, 0);
    }, [])

    const loadData = async () => {
        await fetch(`${BASE_URL}api/v1/cart/get-cart`, {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                id: user.id
            })
        })
            .then(res => res.json())
            .then(res => {
                const tmp = res.cart.map(({ product, ...prev }) => ({
                    ...prev,
                    product: {
                        ...product,
                        image: BASE_URL + product.image
                    }
                }))
                setCart(tmp)
            });
    }

    useEffect(() => {
        loadData();
    }, [])

    return (
        <div className={cx("container")}>
            <div className={cx("header")}>
                <span className={cx("label_1")}>Sản Phẩm</span>
                <span className={cx("label_2")}>Đơn Giá</span>
                <span className={cx("label_3")}>Số Lượng</span>
                <span className={cx("label_4")}>Số Tiền</span>
                <span className={cx("label_5")}>Thao Tác</span>
            </div>
            <div className={cx("content")}>
                {cart.length == 0 && (
                    <div
                        style={{
                            width: 1200,
                            height: 400,
                            display: "flex",
                            alignItems: "center",
                            justifyContent: 'center',
                            fontSize: 32,
                            color: "#e94560",
                            fontWeight: "600"
                        }}>Giỏ hàng đang trống</div>
                )}
                {cart.map(item => (
                    <ItemCart
                        key={item.id}
                        {...item}
                        increase={async () => {
                            console.log("call api")
                            await fetch(`${BASE_URL}api/v1/cart/increase`, {
                                method: "POST",
                                headers: {
                                    Accept: "application/json",
                                    "Content-Type": "application/json"
                                },
                                body: JSON.stringify({
                                    id_user: user.id,
                                    id_product: item.product.id,
                                    number: 1
                                })
                            })
                                .then(res => res.json())
                                .then(res => {
                                    loadData();
                                })
                        }}
                        decrease={async () => {
                            console.log("call api")
                            await fetch(`${BASE_URL}api/v1/cart/decrease`, {
                                method: "POST",
                                headers: {
                                    Accept: "application/json",
                                    "Content-Type": "application/json"
                                },
                                body: JSON.stringify({
                                    id_user: user.id,
                                    id_product: item.product.id,
                                    number: 1
                                })
                            })
                                .then(res => res.json())
                                .then(res => {
                                    loadData();
                                })
                        }} />
                ))}
            </div>
            {cart.length > 0 && (
                <div className={cx("buy")}>
                    <span className={cx("purchase")}>Tổng Thanh Toán:</span>
                    <span
                        className={cx("total")}>
                        {`${money(total())} VND`}
                    </span>
                    <button
                        className={cx("btn-buy")}
                        onClick={() => {
                            if (cart.length > 0) {
                                navigate("/checkout")
                            }
                        }}>
                        Mua Hàng
                    </button>
                </div>
            )}
        </div>
    );
}

function ItemCart({ id, user, product, count, increase, decrease }) {

    const navigate = useNavigate();

    return (
        <div className={cx("wrapper")}>
            <div
                className={cx("product")}
                onClick={() => navigate(`/product/${concatString(product.name)}${product.id}`)}>
                <img
                    className={cx("image")}
                    src={product.image} />
                <span className={cx("name")}>{product.name}</span>
            </div>
            <span className={cx("price")}>{`${money(product.newprice)} VND`}</span>
            <div className={cx("action")}>
                <button
                    className={cx("btn-minus")}
                    onClick={decrease} >
                    <FontAwesomeIcon icon={faMinus} />
                </button>
                <div className={cx("wrapper_input")}>
                    <span>{count}</span>
                </div>
                <button
                    className={cx("btn-plus")}
                    onClick={increase} >
                    <FontAwesomeIcon icon={faPlus} />
                </button>
            </div>
            <span className={cx("cost")}>{`${money(count * product.newprice)} VND`}</span>
            <button className={cx("delete")}>
                Xoá
            </button>
        </div>
    )
}

export default Cart