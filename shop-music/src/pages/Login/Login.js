import { useState } from "react";
import { useNavigate } from "react-router-dom";
import classNames from "classnames/bind";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";

import styles from "./Login.module.scss";
import { BASE_URL } from "../../configs/config";
import Global from "../../global";

const cx = classNames.bind(styles);

function Login() {

    const [show, setShow] = useState(false);
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const navigate = useNavigate();

    const handleLogin = async () => {
        if (!username || !password) {
            return;
        }
        await fetch(`${BASE_URL}api/v1/user/login`, {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                username: username,
                password: password
            })
        })
            .then(res => res.json())
            .then(res => {
                if (res.message == "success") {
                    Global.setUser(res.user);
                    const str = JSON.stringify(res.user);
                    localStorage.setItem("user", str);
                    navigate("/");
                    Global.getFunc("loadCartCount")();
                    Global.getFunc("renderApp")(Math.random());
                }
                else {
                    window.alert(res.reason);
                }
            });
    }

    return (
        <div className={cx("container")}>
            <div className={cx("wrapper")}>
                <span className={cx("title")}>Đăng Nhập</span>
                <div style={{ height: 1, background: "#d4d4d4", marginLeft: 120, marginRight: 155 }}></div>
                <div className={cx("row")}>
                    <span className={cx("label")}>Tài khoản:</span>
                    <div className={cx("border")}>
                        <input
                            className={cx("username")}
                            placeholder="Email/Tên đăng nhập/Số điện thoại"
                            value={username}
                            onChange={e => setUsername(e.target.value)} />
                    </div>
                </div>
                <div className={cx("row")}>
                    <span className={cx("label")}>Mật khẩu:</span>
                    <div className={cx("border")}>
                        <input
                            className={cx("username")}
                            placeholder="Mật khẩu"
                            type={show ? "text" : "password"}
                            value={password}
                            onChange={e => setPassword(e.target.value)} />
                        <button
                            className={cx("show")}
                            onClick={() => setShow(!show)}>
                            <FontAwesomeIcon icon={show ? faEye : faEyeSlash} />
                        </button>
                    </div>
                </div>
                <div className={cx("action")}>
                    <div style={{ display: "flex", alignItems: "center" }}>
                        <input className={cx("checkbox")} type="checkbox" />
                        <span className={cx("remember")}>Ghi nhớ đăng nhập</span>
                    </div>
                    <span className={cx("forget")}>Quên mật khẩu</span>
                </div>
                <button
                    className={cx("btn-login")}
                    onClick={handleLogin} >
                    ĐĂNG NHẬP
                </button>
                <div style={{ alignSelf: "center", marginTop: 20 }}>
                    <span style={{ color: "#777676" }}>Chưa có tài khoản?</span>
                    <span
                        className={cx("signup")}
                        onClick={() => navigate("/signup")}>Đăng ký ngay</span>
                </div>
            </div>
        </div>
    );
}

export default Login