import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import classNames from "classnames/bind"

import styles from "./CheckOut.module.scss";
import Global from "../../global";
import { BASE_URL } from "../../configs/config";

const cx = classNames.bind(styles)

function CheckOut() {

    const user = Global.getUser();
    const navigate = useNavigate();
    const [name, setName] = useState(user.fullname ?? "");
    const [phone, setPhone] = useState(user.phone ?? "");
    const [address, setAddress] = useState(user.address ?? "");
    const [note, setNote] = useState("");

    const handleBuy = async () => {
        await fetch(`${BASE_URL}api/v1/order/create`, {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                id: user.id,
                name,
                phone,
                address,
                note
            })
        })
            .then(res => res.json())
            .then(res => {
                if (res.message == "success") {
                    window.alert("Đặt hàng thành công");
                    Global.getFunc("loadCartCount")();
                    navigate('/');
                }
            });
    }

    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);

    return (
        <div className={cx("container")}>
            <div className={cx("pay")}>Thanh Toán</div>
            <div className={cx("wrapper")}>
                <span className={cx("place")}>Thông tin nhận hàng:</span>
                <div className={cx("_row")}>
                    <div className={cx("row")}>
                        <span className={cx("label")}>Họ và tên người nhận:</span>
                        <div className={cx("wrapper_input")}>
                            <input
                                className={cx("input")}
                                placeholder="Họ và tên"
                                value={name}
                                onChange={e => setName(e.target.value)} />
                        </div>
                    </div>
                    <div className={cx("row")}>
                        <span className={cx("label")}>Số điện thoại:</span>
                        <div className={cx("wrapper_input")}>
                            <input
                                className={cx("input")}
                                placeholder="Số điện thoại"
                                value={phone}
                                onChange={e => setPhone(e.target.value)} />
                        </div>
                    </div>
                </div>
                <div className={cx("row_")}>
                    <span className={cx("label_")}>Địa chỉ nhận hàng:</span>
                    <div className={cx("wrapper_input_")}>
                        <input
                            className={cx("input_")}
                            placeholder="Địa chỉ"
                            value={address}
                            onChange={e => setAddress(e.target.value)} />
                    </div>
                </div>
                <div className={cx("row_")}>
                    <span className={cx("label_")}>Ghi chú đơn hàng:</span>
                    <div className={cx("wrapper_input_")}>
                        <input
                            className={cx("input_")}
                            placeholder="Ghi chú"
                            value={note}
                            onChange={e => setNote(e.target.value)} />
                    </div>
                </div>
                <button
                    className={cx("btn-buy")}
                    onClick={handleBuy}>
                    Đặt Hàng
                </button>
            </div>
        </div >
    );
}

export default CheckOut