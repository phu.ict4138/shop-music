import classNames from "classnames/bind";
import { useSearchParams } from "react-router-dom";

import styles from "./Search.module.scss";
import CardItem from "../../components/CardItem"
import { useEffect, useState } from "react";

import Global from "../../global";
import { BASE_URL } from "../../configs/config";

const cx = classNames.bind(styles);

const pair = (arr) => {
    const _a = [];
    for (let i = 0; i < arr.length / 6; i++) {
        _a.push([arr[i * 6], arr[i * 6 + 1], arr[i * 6 + 2], arr[i * 6 + 3], arr[i * 6 + 4], arr[i * 6 + 5]])
    }
    return _a;
}

function Search() {

    let [searchParams, setSearchParams] = useSearchParams();
    const [data, setData] = useState([]);
    let text = searchParams.get("q");
    console.log(text);

    const search = async () => {
        await fetch(`${BASE_URL}api/v1/search?q=${text}`, {
            method: "GET",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            }
        })
            .then(res => res.json())
            .then(res => {
                const _t = res.data.map(({ id, name, image, price, newprice }) => ({
                    id,
                    name,
                    price,
                    newPrice: newprice,
                    image: BASE_URL + image
                }));
                setData(_t)
            })
    }

    useEffect(() => {
        search()
    }, [text]);

    return (
        <div className={cx("container")}>
            <div className={cx("content")}>
                {pair(data).map((item, index) => (
                    <div key={index} className={cx("row")}>
                        <CardItem {...item[0]} />
                        {item[1] && <CardItem {...item[1]} />}
                        {item[2] && <CardItem {...item[2]} />}
                        {item[3] && <CardItem {...item[3]} />}
                        {item[4] && <CardItem {...item[4]} />}
                        {item[5] && <CardItem {...item[5]} />}
                    </div>
                ))}
                <div className={cx("row")}>
                </div>
                <div className={cx("row")}>
                </div>
                <div className={cx("row")}>
                </div>
                <div className={cx("row")}>
                </div>
            </div>
        </div>
    );
}

export default Search