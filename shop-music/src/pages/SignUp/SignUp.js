import classNames from "classnames/bind";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEye, faEyeSlash } from "@fortawesome/free-solid-svg-icons";

import styles from "./SignUp.module.scss";
import { useState } from "react";
import { BASE_URL } from "../../configs/config";
import { useNavigate } from "react-router-dom";

const cx = classNames.bind(styles);

function SignUp() {

    const [show, setShow] = useState(true);
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [phone, setPhone] = useState("");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");

    const navigate = useNavigate();

    const handleSignUp = async () => {
        await fetch(`${BASE_URL}api/v1/user/signup`, {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                fullname: name,
                email,
                phone,
                username,
                password,
                confirmPassword
            })
        })
            .then(res => res.json())
            .then(res => {
                if (res.message == "success") {
                    window.alert("Đăng ký thành công");
                    navigate("/login")
                }
                else {
                    window.alert(res.reason);
                }
            })
    }

    return (
        <div className={cx("container")}>
            <div className={cx("wrapper")}>
                <span className={cx("title")}>Đăng Ký</span>
                <div style={{ height: 1, background: "#d4d4d4", marginLeft: 120, marginRight: 120 }}></div>
                <div className={cx("row")}>
                    <span className={cx("label")}>Họ Và Tên:</span>
                    <div className={cx("border")}>
                        <input
                            className={cx("username")}
                            placeholder="Họ Và Tên"
                            value={name}
                            onChange={e => setName(e.target.value)} />
                    </div>
                </div>
                <div className={cx("row")}>
                    <span className={cx("label")}>Email:</span>
                    <div className={cx("border")}>
                        <input
                            className={cx("username")}
                            placeholder="Email"
                            onChange={e => setEmail(e.target.value)} />
                    </div>
                </div>
                <div className={cx("row")}>
                    <span className={cx("label")}>Số điện thoại:</span>
                    <div className={cx("border")}>
                        <input
                            className={cx("username")}
                            placeholder="Số điện thoại"
                            onChange={e => setPhone(e.target.value)} />
                    </div>
                </div>
                <div className={cx("row")}>
                    <span className={cx("label")}>Tên đăng nhập:</span>
                    <div className={cx("border")}>
                        <input
                            className={cx("username")}
                            placeholder="Tên đăng nhập"
                            onChange={e => setUsername(e.target.value)} />
                    </div>
                </div>
                <div className={cx("row")}>
                    <span className={cx("label")}>Mật khẩu:</span>
                    <div className={cx("border")}>
                        <input
                            className={cx("username")}
                            placeholder="Mật khẩu"
                            type={show ? "text" : "password"}
                            onChange={e => setPassword(e.target.value)} />
                        <button
                            className={cx("show")}
                            onClick={() => setShow(!show)}>
                            <FontAwesomeIcon icon={show ? faEye : faEyeSlash} />
                        </button>
                    </div>
                </div>
                <div className={cx("row")}>
                    <span className={cx("label")}>Nhập lại mật khẩu:</span>
                    <div className={cx("border")}>
                        <input
                            className={cx("username")}
                            placeholder="Mật khẩu"
                            type={show ? "text" : "password"}
                            onChange={e => setConfirmPassword(e.target.value)} />
                        <button
                            className={cx("show")}
                            onClick={() => setShow(!show)}>
                            <FontAwesomeIcon icon={show ? faEye : faEyeSlash} />
                        </button>
                    </div>
                </div>
                <button
                    className={cx("btn-signup")}
                    onClick={handleSignUp} >
                    ĐĂNG KÝ
                </button>
            </div>
        </div>
    );
}

export default SignUp