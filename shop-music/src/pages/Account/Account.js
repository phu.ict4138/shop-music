import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import classNames from "classnames/bind";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClockRotateLeft, faLocationDot, faPlus, faUser } from "@fortawesome/free-solid-svg-icons";

import styles from "./Account.module.scss";
import image_1 from "../../assets/images/anh1.jpg";
import { BASE_URL, concatString, money } from "../../configs/config";
import Global from "../../global";

const nav = [
    { id: 0, name: "profile", display: "Thông Tin Tài Khoản", icon: faUser, component: <Profile /> },
    { id: 1, name: "address", display: "Địa Chỉ Nhận Hàng", icon: faLocationDot, component: <Address /> },
    { id: 2, name: "history", display: "Lịch Sử Mua Hàng", icon: faClockRotateLeft, component: <History /> },
]

const cx = classNames.bind(styles);

function Account() {

    const { type } = useParams();
    console.log(type)
    const navigate = useNavigate();
    const user = Global.getUser();
    console.log(user)

    const [types, setType] = useState(() => {
        if (type == "profile") {
            return 0;
        }
        if (type == "address") {
            return 1;
        }
        return 2;
    });
    console.log(types)

    return (
        <div className={cx("container")}>
            <div className={cx("nav-bar")}>
                <div className={cx("top")}>
                    <img
                        className={cx("avatar")}
                        src={BASE_URL + user.avatar ?? image_1} />
                    <span className={cx("username")}>{user.username}</span>
                </div>
                {nav.map(item => (
                    <NavItem
                        onClick={() => {
                            navigate(`/account/${item.name}`);
                            setType(item.id)
                        }}
                        type={types}
                        key={item.id}
                        {...item} />
                ))}
            </div>
            <div className={cx("content")}>
                {nav[types].component}
            </div>
        </div>
    );
}

function NavItem({ id, display, icon, type, onClick }) {

    return (
        <div
            className={cx({ "wrapper": true, "active": type == id })}
            onClick={onClick}>
            <span className={cx("nav-icon")}>
                <FontAwesomeIcon icon={icon} />
            </span>
            <span className={cx(("display"))}>{display}</span>
        </div>
    );
}

function Profile() {

    const user = Global.getUser()

    return (
        <div className={cx("profile_wrapper")}>
            <div className={cx("profile_header")}>
                <span className={cx("profile_title")}>Hồ Sơ Của Tôi</span>
            </div>
            <div className={cx("profile_content")}>
                <div className={cx("profile_infor")}>
                    <div className={cx("profile_row")}>
                        <span className={cx("profile_label")}>Tên Đăng Nhập:</span>
                        <span className={cx("profile_username")}>{user.username}</span>
                    </div>
                    <div className={cx("profile_row")}>
                        <span className={cx("profile_label")}>Họ Và Tên:</span>
                        <div className={cx("profile_border")}>
                            <input className={cx("profile_input")} placeholder="Họ vè tên" value={user.fullname} />
                        </div>
                    </div>
                    <div className={cx("profile_row")}>
                        <span className={cx("profile_label")}>Email:</span>
                        <div className={cx("profile_border")}>
                            <input className={cx("profile_input")} placeholder="Email" value={user.email} />
                        </div>
                    </div>
                    <div className={cx("profile_row")}>
                        <span className={cx("profile_label")}>Số điện thoại:</span>
                        <div className={cx("profile_border")}>
                            <input className={cx("profile_input")} placeholder="Trương Quang Phú" value={user.phone} />
                        </div>
                    </div>
                    <div className={cx("profile_row")}>
                        <span className={cx("profile_label")}>Ngày sinh:</span>
                        <div className={cx("profile_border_2")}>
                            <input className={cx("profile_input_2")} placeholder="01" value={user.birthday?.slice(8, 10)} />
                        </div>
                        <span className={cx("profile_space")}>--</span>
                        <div className={cx("profile_border_2")}>
                            <input className={cx("profile_input_2")} placeholder="01" value={user.birthday?.slice(5, 7)} />
                        </div>
                        <span className={cx("profile_space")}>--</span>
                        <div className={cx("profile_border_2")}>
                            <input className={cx("profile_input_2")} placeholder="2000" value={user.birthday?.slice(0, 4)} />
                        </div>
                    </div>
                    <button
                        className={cx("profile_save")}
                        onClick={() => 1} >
                        Lưu
                    </button>
                </div>
                <div className={cx("profile_image")}>
                    <img
                        className={cx("profile_img")}
                        src={BASE_URL + user.avatar ?? image_1} />
                    <button className={cx("profile_choose")}>Chọn Ảnh</button>
                </div>
            </div>
        </div>
    );
}

function Address() {

    return (
        <div className={cx("address_wrapper")}>
            <div className={cx("address_header")}>
                <span className={cx("address_title")}>Địa Chỉ Nhận Hàng</span>
                <button className={cx("address_add")}>
                    <FontAwesomeIcon icon={faPlus} style={{ marginRight: 8 }} />
                    <span>Thêm Địa Chỉ Mới</span>
                </button>
            </div>
        </div>
    );
}

function History() {

    const [histories, setHistories] = useState([]);
    const user = Global.getUser();

    const loadData = async () => {
        await fetch(`${BASE_URL}api/v1/user/history`, {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                id: user.id
            })
        })
            .then(res => res.json())
            .then(res => {
                if (res.message == "success") {
                    setHistories(res.history)
                }
            })
    }

    useEffect(() => {
        loadData()
    }, [])

    return (
        <div className={cx("history_wrapper")}>
            <div className={cx("history_header")}>
                <span className={cx("history_title")}>Lịch sử mua hàng</span>
            </div>
            <div className={cx("history_content")}>
                {histories.map(item => (
                    <ItemHistory
                        key={item.id}
                        {...item} />
                ))}
            </div>
        </div>
    );
}

function ItemHistory({ id, name, phone, address, total, products, status, note }) {

    return (
        <div className={cx("item_history_wrapper")}>
            <div className={cx("item_history_header")}>
                <span>{status}</span>
            </div>
            <div className={cx("item_history_content")}>
                {products.map(item => (
                    <Item
                        key={item.id}
                        {...item} />
                ))}
            </div>
            <div className={cx("item_history_infor")}>
                <div style={{ flex: 1 }}>
                    <div style={{ marginTop: 8 }}>
                        <span style={{ color: "#7e7e7e", marginRight: 4 }}>Họ và tên:</span>
                        <span>{name}</span>
                    </div>
                    <div style={{ marginTop: 4 }}>
                        <span style={{ color: "#7e7e7e", marginRight: 4 }}>SĐT:</span>
                        <span>{phone}</span>
                    </div>
                    <div style={{ marginTop: 4 }}>
                        <span style={{ color: "#7e7e7e", marginRight: 4 }}>Địa chỉ:</span>
                        <span>{address}</span>
                    </div>
                    <div style={{ marginTop: 4, marginBottom: 8 }}>
                        <span style={{ color: "#7e7e7e", marginRight: 4 }}>Ghi chú:</span>
                        <span>{note}</span>
                    </div>
                </div>
                <div
                    style={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "center",
                        justifyContent: "end",
                        paddingRight: 8,
                        width: 300
                    }} >
                    <span
                        style={{
                            color: "#7e7e7e",
                            fontSize: 18
                        }}>
                        Tổng:
                    </span>
                    <span
                        style={{
                            color: "#e94560",
                            fontSize: 18,
                            marginLeft: 8
                        }}>
                        {`${money(total)} VNĐ`}
                    </span>
                </div>
            </div>
        </div>
    )
}

function Item({ id, name, price, newprice, image, count }) {

    const navigate = useNavigate()

    return (
        <div
            className={cx("item_wrapper")}
            onClick={() => navigate(`/product/${concatString(name)}-${id}`)}>
            <img
                className={cx("item_image")}
                src={BASE_URL + image ?? image_1}
            />
            <div className={cx("item_infor")}>
                <span className={cx("item_name")}>{name}</span>
                <div
                    style={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "end",
                        marginTop: 8
                    }}>
                    <div
                        style={{
                            fontSize: 14,
                            marginRight: 4,
                            color: "#7e7e7e"
                        }}>
                        Số lượng:
                    </div>
                    <span style={{ color: "#e94560" }}>{count}</span>
                </div>
            </div>
            <div className={cx("item_left")}>
                <span className={cx("item_price")}>{`${money(price)} VNĐ`}</span>
                <span className={cx("item_newprice")}>{`${money(newprice)} VNĐ`}</span>
            </div>
        </div >
    )
}

export default Account