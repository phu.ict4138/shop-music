import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleLeft, faAngleRight, faCartShopping, faMinus, faPlus } from "@fortawesome/free-solid-svg-icons";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import classNames from "classnames/bind";

import styles from "./Product.module.scss";
import { BASE_URL, money } from "../../configs/config";
import Global from "../../global";

const cx = classNames.bind(styles);

function NextArrow({ onClick }) {
    return (
        <div onClick={onClick} className={cx("next")}>
            <button className={cx("next")}>
                <FontAwesomeIcon icon={faAngleRight} />
            </button>
        </div>
    )
}

function PrevArrow({ onClick }) {
    return (
        <div onClick={onClick} className={cx("prev")}>
            <button className={cx("prev")}>
                <FontAwesomeIcon icon={faAngleLeft} />
            </button>
        </div>
    )
}

function Product() {

    const [image, setImage] = useState("");
    const [number, setNumber] = useState(1);
    const [product, setProduct] = useState({});
    const { name_product } = useParams();
    const arr = name_product.split("-");
    const id_product = arr[arr.length - 1];
    const user = Global.getUser();

    const loadData = async () => {
        await fetch(`${BASE_URL}api/v1/product/${id_product}`, {
            method: "GET",
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        })
            .then(res => res.json())
            .then(res => {
                const { id, name, price, newprice, country, image, images, category, origin } = res.product;
                const _x = {
                    id,
                    name,
                    price,
                    image,
                    newprice,
                    country,
                    images: images.map(item => (BASE_URL + item)),
                    category,
                    origin
                }
                setImage(_x.images[0]);
                setProduct(_x);
            })
    }

    const addToCart = async () => {
        if (!user.id) {
            window.alert("Bạn cần đăng nhập");
            return;
        }
        await fetch(`${BASE_URL}api/v1/cart/increase`, {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                id_user: user.id,
                id_product: product.id,
                number: number
            })
        })
            .then(res => res.json())
            .then(res => {
                Global.getFunc("loadCartCount")();
            })
    }

    useEffect(() => {
        loadData();
        window.scrollTo(0, 0);
    }, []);


    return (
        <div className={cx("container")}>
            <div className={cx("content")}>
                <div className={cx("left")}>
                    <img
                        className={cx("image")}
                        src={image ? image : ""}
                        alt="hi" />
                    <div className={cx("inner")}>
                        <Slider
                            slidesToShow={product.images?.length >= 4 ? 4 : product.images?.length || 4}
                            slidesToScroll={1}
                            autoplay={true}
                            autoplaySpeed={4000}
                            styles={{ backGroundColor: "red" }}
                            nextArrow={<NextArrow />}
                            prevArrow={<PrevArrow />} >
                            {product.images && product.images.map((item, index) => (
                                <div key={index}>
                                    <img
                                        onClick={() => setImage(item)}
                                        onMouseOver={() => setImage(item)}
                                        className={cx("img")}
                                        src={item}
                                        alt="hi" />
                                </div>
                            ))}
                        </Slider>
                    </div>
                </div>
                <div className={cx("right")}>
                    <h2 className={cx("name")}>{product.name}</h2>
                    <div className={cx("row")}>
                        <span className={cx("label_1")}>Tình trạng:</span>
                        <span className={cx("conđiton")}>Còn hàng</span>
                    </div>
                    <span className={cx("price")}>{`${money(product.price || 30)} VND`}</span>
                    <span className={cx("new-price")}>{`${money(product.newprice || 30)} VND`}</span>
                    <div className={cx("wrapper")}>
                        <span className={cx("label_2")}>Số lượng:</span>
                        <div className={cx("action")}>
                            <button
                                className={cx("btn-minus")}
                                onClick={() => {
                                    if (number > 1) {
                                        setNumber(number - 1);
                                    }
                                }}>
                                <FontAwesomeIcon icon={faMinus} />
                            </button>
                            <div className={cx("wrapper_input")}>
                                <input
                                    className={cx("number")}
                                    placeholder="1"
                                    type="text"
                                    value={number}
                                    onChange={e => setNumber(e.target.value)} />
                            </div>
                            <button
                                className={cx("btn-plus")}
                                onClick={() => {
                                    if (number < 9) {
                                        setNumber(number + 1);
                                    }
                                }}>
                                <FontAwesomeIcon icon={faPlus} />
                            </button>
                        </div>
                    </div>
                    <button onClick={addToCart} className={cx("btn-add")}>
                        <FontAwesomeIcon icon={faCartShopping} />
                        <span className={cx("add")}>Thêm Vào Giỏ Hàng</span>
                    </button>
                </div>
            </div>
            <div className={cx("infor")}>
                <h2 className={cx("infor_1")}>Thông Tin Sản Phẩm</h2>
                <div className={cx("wrapper_1")}>
                    <span className={cx("title_1")}>Hãng sản xuất: </span>
                    <span className={cx("value")}>{product.origin?.name || "??"}</span>
                </div>
                <div className={cx("wrapper_1")}>
                    <span className={cx("title_1")}>Xuất xứ:       </span>
                    <span className={cx("value")}>Mỹ</span>
                </div>
                <div className={cx("wrapper_1")}>
                    <span className={cx("title_1")}>Loại sản phẩm:</span>
                    <span className={cx("value")}>{product.category?.name || "??"}</span>
                </div>
                <span>Video đánh giá:</span>
                <div className={cx("wrapper_2")}>
                    <iframe width="1000" height="600" src="https://www.youtube.com/embed/JD1T36Kt-m8"></iframe>
                </div>
            </div>
        </div>
    );
}

export default Product