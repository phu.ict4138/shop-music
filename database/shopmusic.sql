-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th9 02, 2022 lúc 05:28 AM
-- Phiên bản máy phục vụ: 10.4.24-MariaDB
-- Phiên bản PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `shopmusic`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `cart`
--

CREATE TABLE `cart` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `cart`
--

INSERT INTO `cart` (`id`, `id_user`, `id_product`, `count`) VALUES
(33, 15, 2, 1),
(34, 15, 3, 8),
(50, 2, 5, 3),
(51, 2, 6, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `category`
--

INSERT INTO `category` (`id`, `name`, `description`, `logo`) VALUES
(3, 'Đàn Guitar Ascoutic', NULL, 'upload/images/2ps01nk9jphhkmzp6qg0.png'),
(4, 'Đàn Guitar Classic', NULL, 'upload/images/xsta9hdi5uj8x7a10we1.png'),
(5, 'Đàn Ukulele', NULL, 'upload/images/iiccn5twgksdzwi7al3r.png'),
(6, 'Dây đàn Guitar Ascoutic', NULL, 'upload/images/bsl2drzhfvgi96j504o4.png'),
(7, 'Dây đàn Guitar Classic', NULL, 'upload/images/jz14eqv2nba2mz2xbvc4.png'),
(8, 'Capo', NULL, 'upload/images/0fer4ptc2dpluah5q76v.png'),
(9, 'Bao đàn Guitar', NULL, 'upload/images/evi6wijeu00378x4elc6.png'),
(10, 'Bao đàn Ukulele', NULL, 'upload/images/zedf54lo9wskgoppkz6g.png'),
(11, 'Trống Cajon', NULL, 'upload/images/fmy470bzrxsf9atql8sj.png'),
(12, 'Đàn Guitar Điện', NULL, 'upload/images/x1ui6ujynnf1suhvhk3a.png'),
(13, 'Dây đàn guitar Điện', NULL, NULL),
(14, 'test name edit 5', NULL, 'image.com'),
(17, 'test create category 2`', NULL, 'ok'),
(18, 'test create category 3`', NULL, 'ok'),
(19, 'test create category 4`', NULL, 'ok'),
(20, 'test create category 5', 'hi', 'ok');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `images`
--

CREATE TABLE `images` (
  `id` int(11) NOT NULL,
  `id_resource` int(11) NOT NULL,
  `link` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `images`
--

INSERT INTO `images` (`id`, `id_resource`, `link`) VALUES
(1, 2, 'upload/images/17g5s5lsdkm2jayujx1f.png'),
(2, 2, 'upload/images/quv7j9t79bfpmxvwyba2.png'),
(3, 2, 'upload/images/n5pi7kp6i128xmjd6sxv.png'),
(4, 2, 'upload/images/6ohtk4clxn0djkrf5qed.png'),
(313, 3, 'upload/images/2q27e9vhuhzxo31w32en.png'),
(314, 3, 'upload/images/4es8hfiy4fjqq5c12d9t.png'),
(315, 3, 'upload/images/92r97v6z2teg4xi1w6xo.png'),
(316, 3, 'upload/images/n1nfg2xmstci4qkhyey5.png'),
(317, 5, 'upload/images/dt8y6y6freg736zvcfid.png'),
(318, 5, 'upload/images/g9vcajpbd019wqkslsto.png'),
(319, 5, 'upload/images/k52x1fl3cmuqlu0imqnr.png'),
(320, 5, 'upload/images/lrs0nl898pcytqsaastg.png'),
(321, 5, 'upload/images/a444h8cv62bt150m130v.png'),
(322, 6, 'upload/images/s01awq7d424fyt0v0loa.png'),
(323, 6, 'upload/images/7kbuvzkgcde6zjecmj9h.png'),
(324, 6, 'upload/images/9x36d3xnd210g3r4dfrg.png'),
(325, 6, 'upload/images/6wpvsqvva10pq3bpsf6l.png'),
(326, 6, 'upload/images/fm44bkd4ef5gvga8bf24.png'),
(327, 7, 'upload/images/ypz2d8mux20ov21625le.png'),
(328, 7, 'upload/images/muuqyl1e9sfmeu8he7b0.png'),
(329, 7, 'upload/images/hzffc5ia91tatxir8ltj.png'),
(330, 7, 'upload/images/w36b4lnuwx8p3enen8fn.png'),
(331, 7, 'upload/images/fna9tp50qta34lvci7cb.png'),
(332, 8, 'upload/images/m07feo5uosu78ixb5rl7.png'),
(333, 8, 'upload/images/km41116u031ilv0zm5lp.png'),
(334, 8, 'upload/images/9z8wnwlr7d89bfulht7e.png'),
(336, 31, 'upload/images/rov5k8fq8ii341pkavnq.png'),
(337, 31, 'upload/images/5u92r2uxksb9b27o3cv4.png'),
(338, 31, 'upload/images/8hp8gc3ejruxz9lff5qg.png'),
(339, 31, 'upload/images/935px2kp1btp0c1ip46t.png'),
(340, 31, 'upload/images/aoudy35nflbfl3sf9oui.png'),
(341, 32, 'upload/images/zu3c2wh0u73gw0r6afp5.png'),
(342, 32, 'upload/images/3i9pip39b2d962zprrk9.png'),
(343, 32, 'upload/images/e3q1d425iw5o9u1z8vbh.png'),
(344, 32, 'upload/images/s1xp4mbuqzak747lj0kz.png'),
(345, 32, 'upload/images/nlhwwofcg987q5k9cf6d.png'),
(346, 32, 'upload/images/tqjmmolvalg2nylh1e27.png'),
(347, 33, 'upload/images/f8kvxz1ucjkct9v78y41.png'),
(348, 33, 'upload/images/mg1kb2fr5osm0dgv0ei5.png'),
(349, 33, 'upload/images/fco2fzdwjzowhvfjxlyr.png'),
(351, 33, 'upload/images/pc7dpcrroapq8mtd3v7o.png'),
(352, 34, 'upload/images/3x5kepd9612ewjfi7dnm.png'),
(353, 34, 'upload/images/adgnd05uspsbh3a14m51.png'),
(354, 34, 'upload/images/p1e7q5gh3e62f0b2zvov.png'),
(355, 34, 'upload/images/pwqzg4efku0oxujhtfxl.png'),
(356, 34, 'upload/images/r3dugomx5qyh0ozohth4.png'),
(357, 35, 'upload/images/k32xvgivle5ar3s32ylc.png'),
(358, 35, 'upload/images/xcmvz4wf3f4lcd7hro3l.png'),
(359, 35, 'upload/images/oc78159lanf21pmb35cm.png'),
(360, 35, 'upload/images/y8rsx9r9wdl6gr1v7zfr.png'),
(361, 35, 'upload/images/8lw1woqbsochvc2oysze.png'),
(362, 36, 'upload/images/56t1gdtxgj6td8w3syi.png'),
(363, 37, 'upload/images/n584x8l3nfajcjnt0rs6.png'),
(364, 37, 'upload/images/vp0juwiap4yz0id61ebh.png'),
(365, 37, 'upload/images/b3oo94a4lkfgzlm2hkx7.png'),
(366, 37, 'upload/images/g9ywywabvddyl17pfh2f.png'),
(367, 37, 'upload/images/nuyjnk257moc23kecpol.png'),
(368, 37, 'upload/images/ttokyfuhmxska9j59wee.png');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `createat` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `orders`
--

INSERT INTO `orders` (`id`, `id_user`, `name`, `address`, `phone`, `note`, `total`, `status`, `createat`) VALUES
(9, 2, 'Phú', 'Tp Hồ Chí Minh', '099238213', 'okkkkkkk', 180140000, 'Chờ xác nhận', '2022-07-18 12:40:33'),
(10, 2, 'Phú', 'Tp Hồ Chí Minh', '099238213', 'Test', 180140000, 'Chờ xác nhận', '2022-07-31 12:49:25'),
(11, 2, 'Trương Quang Phú', 'Hai Bà Trưng, Hà Nội', '0987654321', '', 9140000, 'Chờ xác nhận', '2022-07-31 13:13:39'),
(12, 2, 'Trương Quang Phú', 'Hai Bà Trưng, Hà Nội', '0987654321', 'Mua hàng lần đầu', 9140000, 'Chờ xác nhận', '2022-07-31 16:21:14'),
(13, 2, 'Trương Quang Phú', 'Hai Bà Trưng, Hà Nội', '0987654321', 'adasgdasdsadhsad ádhashdsak ádhas hadhaskh ', 150000000, 'Chờ xác nhận', '2022-07-31 16:33:43'),
(14, 2, 'Trương Quang Phú', 'Hai Bà Trưng, Hà Nội', '0987654321', '', 150000000, 'Chờ xác nhận', '2022-07-31 16:36:56'),
(15, 2, 'Trương Quang Phú', 'Hai Bà Trưng, Hà Nội', '0987654321', '', 66420000, 'Chờ xác nhận', '2022-07-31 16:42:42'),
(16, 14, 'Tiến Huy', 'Hai Bà Trưng, Hà Nội', '01312321', 'Mua hàng lần đàu tiên', 95720000, 'Chờ xác nhận', '2022-07-31 17:01:03'),
(17, 15, 'Trần Hải Nam', 'Hà Nội', '0812371321', 'ok', 9140000, 'Chờ xác nhận', '2022-07-31 17:17:25'),
(18, 15, 'Trần Hải Nam', 'Hà Nội', '0812371321', '', 156600000, 'Chờ xác nhận', '2022-07-31 18:55:04'),
(19, 16, 'Trần Tiến Huy', 'Hà Nội', '0967121324', 'ok', 3440000, 'Chờ xác nhận', '2022-08-01 07:23:09'),
(20, 2, 'Trương Quang Phú', 'Hai Bà Trưng, Hà Nội, VN', '0987654321', '213123', 138000, 'Chờ xác nhận', '2022-08-01 11:55:51'),
(21, 18, 'quan', 'Hà Nội', '0123456', 'DFASD', 47300000, 'Chờ xác nhận', '2022-08-09 19:51:26');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order_detail`
--

CREATE TABLE `order_detail` (
  `id` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `note` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `order_detail`
--

INSERT INTO `order_detail` (`id`, `id_order`, `id_product`, `count`, `note`) VALUES
(2, 9, 2, 2, NULL),
(3, 9, 3, 1, NULL),
(4, 9, 6, 1, NULL),
(5, 10, 2, 2, NULL),
(6, 10, 6, 1, NULL),
(7, 10, 3, 1, NULL),
(8, 11, 7, 1, NULL),
(9, 11, 8, 1, NULL),
(10, 12, 7, 1, NULL),
(11, 12, 8, 1, NULL),
(12, 13, 7, 2, NULL),
(13, 13, 8, 2, NULL),
(14, 13, 5, 1, NULL),
(15, 13, 6, 1, NULL),
(16, 13, 2, 1, NULL),
(17, 13, 3, 1, NULL),
(18, 14, 6, 1, NULL),
(19, 14, 7, 2, NULL),
(20, 14, 8, 2, NULL),
(21, 14, 5, 1, NULL),
(22, 14, 3, 1, NULL),
(23, 14, 2, 1, NULL),
(24, 15, 2, 1, NULL),
(25, 16, 2, 1, NULL),
(26, 16, 3, 1, NULL),
(27, 17, 7, 1, NULL),
(28, 17, 8, 1, NULL),
(29, 18, 2, 1, NULL),
(30, 18, 7, 2, NULL),
(31, 18, 5, 3, NULL),
(32, 18, 3, 1, NULL),
(33, 19, 7, 1, NULL),
(34, 20, 32, 1, NULL),
(35, 20, 33, 2, NULL),
(36, 20, 34, 1, NULL),
(37, 21, 3, 1, NULL),
(38, 21, 5, 1, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `origin`
--

CREATE TABLE `origin` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `origin`
--

INSERT INTO `origin` (`id`, `name`, `description`, `country`, `logo`) VALUES
(1, 'Taylor', NULL, NULL, 'upload/images/3vu1kpb6hz5uk4x252uj.png'),
(2, 'Yamaha', NULL, NULL, 'upload/images/27tx0rc08h4s7jm1yule.png'),
(3, 'Thuận Guitar', NULL, NULL, 'upload/images/ba9eajd4tgn50zmaea17.png'),
(4, 'Guitar Trần', NULL, NULL, 'upload/images/po8zqy5k07stwl70ji6g.png'),
(5, 'Martin', NULL, NULL, 'upload/images/pongz0dsq6ivv4phe368.png'),
(6, 'Guitar Ba Đờn', NULL, NULL, 'upload/images/8yj7eezasn2xfrr8shhw.png'),
(7, 'Alice', NULL, NULL, 'upload/images/mrxyfhr2sago4lnxj7r7.png'),
(8, 'Elixir', NULL, NULL, 'upload/images/uj4emhi0jiyi1v3vln6e.png'),
(9, 'D\'Addario', NULL, NULL, 'upload/images/fchapmvg41g76hq0nnbx.png'),
(10, 'Suzuki', NULL, NULL, 'upload/images/mfhlt28y8z4s46lwrnw7.png'),
(11, 'Ayers', NULL, NULL, 'upload/images/a2se202xs3avyjsz846i.png'),
(12, 'Rosen', NULL, NULL, 'upload/images/gnu9yfa642k295pc3w65.png');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `id_category` int(11) DEFAULT NULL,
  `id_origin` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `newprice` int(11) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `product`
--

INSERT INTO `product` (`id`, `name`, `id_category`, `id_origin`, `price`, `newprice`, `country`, `description`, `image`) VALUES
(2, 'Đàn guitar Taylor 414C', 3, 1, 71800000, 66420000, 'Mỹ', NULL, 'upload/images/17g5s5lsdkm2jayujx1f.png'),
(3, 'Đàn guitar Taylor 315CE', 3, 1, 33000000, 29300000, NULL, NULL, 'upload/images/2q27e9vhuhzxo31w32en.png'),
(5, 'Đàn guitar Taylor 110E', 3, 1, 19470000, 18000000, NULL, NULL, 'upload/images/dt8y6y6freg736zvcfid.png'),
(6, 'Đàn guitar Taylor 114E', 3, 1, 19450000, 18000000, NULL, NULL, 'upload/images/s01awq7d424fyt0v0loa.png'),
(7, 'Đàn guitar Yamaha F310', 3, 2, 3600000, 3440000, NULL, NULL, 'upload/images/ypz2d8mux20ov21625le.png'),
(8, 'Đàn guitar Yamaha FG 350D', 3, 2, 6000000, 5700000, NULL, NULL, 'upload/images/m07feo5uosu78ixb5rl7.png'),
(9, 'Đàn guitar Yamaha APX 700', 3, 2, NULL, NULL, NULL, NULL, NULL),
(10, 'Đàn guitar Thuận AT02CX Koa Custom', 3, 3, NULL, NULL, NULL, NULL, NULL),
(11, 'Đàn guitar Thuận DT02C 2020', 3, 3, NULL, NULL, NULL, NULL, NULL),
(12, 'Đàn guitar Thuận AT02 2020', 3, 3, NULL, NULL, NULL, NULL, NULL),
(13, 'Đàn guitar Thuận AT06C Custom', 3, 3, NULL, NULL, NULL, NULL, NULL),
(14, 'Đàn guitar Martin DXMAE', 3, 5, NULL, NULL, NULL, NULL, NULL),
(15, 'Đàn guitar Martin Ed Sheeran', 3, 5, NULL, NULL, NULL, NULL, NULL),
(16, 'Đàn guitar Martin D16RGT', 3, 5, NULL, NULL, NULL, NULL, NULL),
(17, 'Đàn guitar Martin DX2AE Macassar', 3, 5, NULL, NULL, NULL, NULL, NULL),
(18, 'Đàn guitar Martin LX1E', 3, 5, NULL, NULL, NULL, NULL, NULL),
(19, 'Đàn guitar Martin DX1RAE', 3, 5, NULL, NULL, NULL, NULL, NULL),
(20, 'Đàn guitar Martin DX1AE', 3, 5, NULL, NULL, NULL, NULL, NULL),
(21, 'Đàn guitar Martin DRS2', 3, 5, NULL, NULL, NULL, NULL, NULL),
(22, 'Đàn guitar Martin D16GT', 3, 5, NULL, NULL, NULL, NULL, NULL),
(23, 'Đàn guitar Martin DXK2AE', 3, 5, NULL, NULL, NULL, NULL, NULL),
(24, 'Đàn guitar Taylor GS Mini', 3, 1, NULL, NULL, NULL, NULL, NULL),
(25, 'Đàn guitar Taylor GS Mini-e Koa', 3, 1, NULL, NULL, NULL, NULL, NULL),
(26, 'Đàn guitar Rosen G11', 3, 12, NULL, NULL, NULL, NULL, NULL),
(27, 'Đàn guitar Rosen G12', 3, 12, NULL, NULL, NULL, NULL, NULL),
(28, 'Đàn guitar Rosen G13', 3, 12, NULL, NULL, NULL, NULL, NULL),
(29, 'Đàn guitar Rosen G15', 3, 12, NULL, NULL, NULL, NULL, NULL),
(30, 'Đàn guitar Rosen R135', 3, 12, NULL, NULL, NULL, NULL, NULL),
(31, 'Capo cá mập', 8, 7, 40000, 32000, NULL, NULL, 'upload/images/rov5k8fq8ii341pkavnq.png'),
(32, 'Capo kim loại cao cấp', 8, 7, 120000, 70000, NULL, NULL, 'upload/images/zu3c2wh0u73gw0r6afp5.png'),
(33, 'Capo cho đàn guitar', 8, 7, 30000, 25000, NULL, NULL, 'upload/images/f8kvxz1ucjkct9v78y41.png'),
(34, 'Capo ukulele', 10, 7, 25000, 18000, NULL, NULL, 'upload/images/3x5kepd9612ewjfi7dnm.png'),
(35, 'Đàn guitar Classic Ba Đờn C450J', 7, 6, 5500000, 5200000, NULL, NULL, 'upload/images/k32xvgivle5ar3s32ylc.png'),
(36, 'Đàn guitar Classic Ba Đờn C170', 7, 6, 2400000, 2200000, NULL, NULL, 'upload/images/56t1gdtxgj6td8w3syi.png'),
(37, 'Đàn guitar Classic Yamaha CG-TA', 4, 2, 16200000, 15000000, NULL, NULL, 'upload/images/n584x8l3nfajcjnt0rs6.png'),
(39, 'test edit 2', 3, 4, 30000, 27000, NULL, NULL, 'ok'),
(41, 'test create', 3, 3, NULL, NULL, NULL, NULL, NULL),
(42, 'test create', 3, 3, NULL, NULL, NULL, NULL, NULL),
(43, 'test create', 3, 3, NULL, NULL, NULL, NULL, NULL),
(44, 'test create', 3, 3, NULL, NULL, NULL, NULL, NULL),
(45, 'test create', 3, 3, NULL, NULL, NULL, NULL, NULL),
(46, 'test create 2', 3, 3, NULL, NULL, NULL, NULL, NULL),
(47, 'test create 5', 3, 3, NULL, NULL, NULL, NULL, NULL),
(48, 'test create 7', 3, 3, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `fullname`, `phone`, `email`, `address`, `birthday`, `avatar`) VALUES
(2, 'phu', '123456', 'Trương Quang Phú', '0987654321', 'phu@gmail.com', 'Hai Bà Trưng, Hà Nội', '2022-07-13 00:00:00', 'upload/images/avatar_default.png'),
(4, 'huy123', '123', 'Huy', '123213123', 'haha@gmail.com', 'Haf Nooii', NULL, NULL),
(14, 'huyvip123', '123456', 'Tiến Huy', '01312321', 'huy@gmail.com', NULL, NULL, 'upload/images/avatar_default.png'),
(15, 'nam', '123456', 'Trần Hải Nam', '0812371321', 'nam@gmil.com', 'Hà Nội', '2022-07-31 17:16:53', 'upload/images/avatar_default.png'),
(16, 'tienhuy', '123456', 'Trần Tiến Huy', '0967121324', 'húadsa@gmail.com', 'Hà Nội', '2022-08-01 07:21:36', 'upload/images/avatar_default.png'),
(17, 'chien', '123456', 'Lê Quang Chiến', '0921321216', 'chien@gmail.com', 'Hà Nội', '2022-08-01 08:12:38', 'upload/images/avatar_default.png'),
(18, 'quantest', 'quantest', 'quan', '0123456', 'ngotrongquan116@', 'Hà Nội', '2022-08-09 19:48:24', 'upload/images/avatar_default.png'),
(19, 'test signup', '123456', NULL, NULL, NULL, NULL, '2022-02-02 00:00:00', 'upload/images/avatar_default.png'),
(20, 'signup2', '123456', 'Test sign up', '0987123123', 'testeamil@gmai.com', NULL, '2022-02-02 00:00:00', 'upload/images/avatar_default.png'),
(21, 'signup23', '123456', 'Test sign up', '0987123223', 'testeamil@gmai.com', NULL, '2022-02-02 00:00:00', 'upload/images/avatar_default.png');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_product` (`id_product`);

--
-- Chỉ mục cho bảng `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Chỉ mục cho bảng `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_resource` (`id_resource`),
  ADD KEY `id` (`id`),
  ADD KEY `id_2` (`id`),
  ADD KEY `id_resource_2` (`id_resource`);

--
-- Chỉ mục cho bảng `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Chỉ mục cho bảng `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `id_order` (`id_order`),
  ADD KEY `id_product` (`id_product`);

--
-- Chỉ mục cho bảng `origin`
--
ALTER TABLE `origin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Chỉ mục cho bảng `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `id_category` (`id_category`),
  ADD KEY `id_origin` (`id_origin`),
  ADD KEY `id_2` (`id`),
  ADD KEY `id_3` (`id`),
  ADD KEY `id_category_2` (`id_category`),
  ADD KEY `id_origin_2` (`id_origin`);

--
-- Chỉ mục cho bảng `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT cho bảng `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT cho bảng `images`
--
ALTER TABLE `images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=369;

--
-- AUTO_INCREMENT cho bảng `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT cho bảng `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT cho bảng `origin`
--
ALTER TABLE `origin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT cho bảng `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT cho bảng `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `cart_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `cart_ibfk_2` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`);

--
-- Các ràng buộc cho bảng `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `images_ibfk_1` FOREIGN KEY (`id_resource`) REFERENCES `product` (`id`);

--
-- Các ràng buộc cho bảng `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Các ràng buộc cho bảng `order_detail`
--
ALTER TABLE `order_detail`
  ADD CONSTRAINT `order_detail_ibfk_1` FOREIGN KEY (`id_order`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `order_detail_ibfk_2` FOREIGN KEY (`id_product`) REFERENCES `product` (`id`);

--
-- Các ràng buộc cho bảng `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`id_origin`) REFERENCES `origin` (`id`),
  ADD CONSTRAINT `product_ibfk_2` FOREIGN KEY (`id_category`) REFERENCES `category` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
